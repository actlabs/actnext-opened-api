require 'watir'

browser = Watir::Browser.new(:chrome)

@hf = [
	{
		:label => 'H.A.MATH.SP.DS Descriptive Statistics',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.SP.DS.ABD.L1.1__H.A.MATH.SP.DS.ABD.L1.2__H.A.MATH.SP.DS.ABD.L1.3__H.A.MATH.SP.DS.ABD.L1.4__H.A.MATH.SP.DS.ABD.L1.5__H.A.MATH.SP.DS.ABD.L1.6__H.A.MATH.SP.DS.ABD.L2.1__H.A.MATH.SP.DS.ABD.L2.2__H.A.MATH.SP.DS.ABD.L2.3__H.A.MATH.SP.DS.ABD.L2.4__H.A.MATH.SP.DS.ABD.L2.5__H.A.MATH.SP.DS.ABD.L2.6__H.A.MATH.SP.DS.ABD.L3.1__H.A.MATH.SP.DS.ABD.L3.10__H.A.MATH.SP.DS.ABD.L3.11__H.A.MATH.SP.DS.ABD.L3.12__H.A.MATH.SP.DS.ABD.L3.13__H.A.MATH.SP.DS.ABD.L3.14__H.A.MATH.SP.DS.ABD.L3.15__H.A.MATH.SP.DS.ABD.L3.2__H.A.MATH.SP.DS.ABD.L3.3__H.A.MATH.SP.DS.ABD.L3.4__H.A.MATH.SP.DS.ABD.L3.5__H.A.MATH.SP.DS.ABD.L3.6__H.A.MATH.SP.DS.ABD.L3.7__H.A.MATH.SP.DS.ABD.L3.8__H.A.MATH.SP.DS.ABD.L3.9__H.A.MATH.SP.DS.ABD.L4.1__H.A.MATH.SP.DS.ABD.L4.2__H.A.MATH.SP.DS.ABD.L4.3__H.A.MATH.SP.DS.ABD.L4.4__H.A.MATH.SP.DS.ABD.L4.5__H.A.MATH.SP.DS.ABD.L4.6__H.A.MATH.SP.DS.ABD.L4.7__H.A.MATH.SP.DS.DD.L1.1__H.A.MATH.SP.DS.DD.L1.2__H.A.MATH.SP.DS.DD.L1.3__H.A.MATH.SP.DS.DD.L1.4__H.A.MATH.SP.DS.DD.L2.1__H.A.MATH.SP.DS.DD.L2.2__H.A.MATH.SP.DS.DD.L2.3__H.A.MATH.SP.DS.DD.L2.4__H.A.MATH.SP.DS.DD.L2.5__H.A.MATH.SP.DS.DD.L4.1__H.A.MATH.SP.DS.DD.L4.2__H.A.MATH.SP.DS.DD.L4.3__H.A.MATH.SP.DS.DD.L4.4__H.A.MATH.SP.DS.RUV.L1.1__H.A.MATH.SP.DS.RUV.L1.10__H.A.MATH.SP.DS.RUV.L1.11__H.A.MATH.SP.DS.RUV.L1.12__H.A.MATH.SP.DS.RUV.L1.13__H.A.MATH.SP.DS.RUV.L1.14__H.A.MATH.SP.DS.RUV.L1.15__H.A.MATH.SP.DS.RUV.L1.16__H.A.MATH.SP.DS.RUV.L1.17__H.A.MATH.SP.DS.RUV.L1.18__H.A.MATH.SP.DS.RUV.L1.19__H.A.MATH.SP.DS.RUV.L1.2__H.A.MATH.SP.DS.RUV.L1.20__H.A.MATH.SP.DS.RUV.L1.21__H.A.MATH.SP.DS.RUV.L1.22__H.A.MATH.SP.DS.RUV.L1.23__H.A.MATH.SP.DS.RUV.L1.24__H.A.MATH.SP.DS.RUV.L1.3__H.A.MATH.SP.DS.RUV.L1.4__H.A.MATH.SP.DS.RUV.L1.5__H.A.MATH.SP.DS.RUV.L1.6__H.A.MATH.SP.DS.RUV.L1.7__H.A.MATH.SP.DS.RUV.L1.8__H.A.MATH.SP.DS.RUV.L1.9__H.A.MATH.SP.DS.RUV.L2.1__H.A.MATH.SP.DS.RUV.L2.10__H.A.MATH.SP.DS.RUV.L2.11__H.A.MATH.SP.DS.RUV.L2.12__H.A.MATH.SP.DS.RUV.L2.2__H.A.MATH.SP.DS.RUV.L2.3__H.A.MATH.SP.DS.RUV.L2.4__H.A.MATH.SP.DS.RUV.L2.5__H.A.MATH.SP.DS.RUV.L2.6__H.A.MATH.SP.DS.RUV.L2.7__H.A.MATH.SP.DS.RUV.L2.8__H.A.MATH.SP.DS.RUV.L2.9'
	},
	{
		:label => 'H.A.MATH.SP.IS Inferential Statistics',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.SP.IS.MIP.L1.1__H.A.MATH.SP.IS.MIP.L1.10__H.A.MATH.SP.IS.MIP.L1.11__H.A.MATH.SP.IS.MIP.L1.12__H.A.MATH.SP.IS.MIP.L1.13__H.A.MATH.SP.IS.MIP.L1.14__H.A.MATH.SP.IS.MIP.L1.15__H.A.MATH.SP.IS.MIP.L1.16__H.A.MATH.SP.IS.MIP.L1.17__H.A.MATH.SP.IS.MIP.L1.18__H.A.MATH.SP.IS.MIP.L1.19__H.A.MATH.SP.IS.MIP.L1.2__H.A.MATH.SP.IS.MIP.L1.20__H.A.MATH.SP.IS.MIP.L1.21__H.A.MATH.SP.IS.MIP.L1.22__H.A.MATH.SP.IS.MIP.L1.3__H.A.MATH.SP.IS.MIP.L1.4__H.A.MATH.SP.IS.MIP.L1.5__H.A.MATH.SP.IS.MIP.L1.6__H.A.MATH.SP.IS.MIP.L1.7__H.A.MATH.SP.IS.MIP.L1.8__H.A.MATH.SP.IS.MIP.L1.9__H.A.MATH.SP.IS.MIP.L2.1__H.A.MATH.SP.IS.MIP.L2.2__H.A.MATH.SP.IS.MIP.L2.3__H.A.MATH.SP.IS.MIP.L2.4__H.A.MATH.SP.IS.MIP.L2.5__H.A.MATH.SP.IS.MIP.L2.6__H.A.MATH.SP.IS.ST.L1.1__H.A.MATH.SP.IS.ST.L1.2__H.A.MATH.SP.IS.ST.L1.3__H.A.MATH.SP.IS.ST.L1.4__H.A.MATH.SP.IS.ST.L2.1__H.A.MATH.SP.IS.ST.L2.10__H.A.MATH.SP.IS.ST.L2.11__H.A.MATH.SP.IS.ST.L2.12__H.A.MATH.SP.IS.ST.L2.13__H.A.MATH.SP.IS.ST.L2.2__H.A.MATH.SP.IS.ST.L2.3__H.A.MATH.SP.IS.ST.L2.4__H.A.MATH.SP.IS.ST.L2.5__H.A.MATH.SP.IS.ST.L2.6__H.A.MATH.SP.IS.ST.L2.7__H.A.MATH.SP.IS.ST.L2.8__H.A.MATH.SP.IS.ST.L2.9'
	},
	{
		:label => 'H.A.MATH.SP.P Probability',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.SP.P.DRV.L1.1__H.A.MATH.SP.P.DRV.L1.2__H.A.MATH.SP.P.DRV.L1.3__H.A.MATH.SP.P.DRV.L1.4__H.A.MATH.SP.P.DRV.L1.5__H.A.MATH.SP.P.DRV.L2.1__H.A.MATH.SP.P.DRV.L2.2__H.A.MATH.SP.P.DRV.L2.3__H.A.MATH.SP.P.DRV.L2.4__H.A.MATH.SP.P.DRV.L2.5__H.A.MATH.SP.P.DRV.L2.6__H.A.MATH.SP.P.FP.L1.1__H.A.MATH.SP.P.FP.L1.10__H.A.MATH.SP.P.FP.L1.11__H.A.MATH.SP.P.FP.L1.12__H.A.MATH.SP.P.FP.L1.13__H.A.MATH.SP.P.FP.L1.14__H.A.MATH.SP.P.FP.L1.15__H.A.MATH.SP.P.FP.L1.16__H.A.MATH.SP.P.FP.L1.17__H.A.MATH.SP.P.FP.L1.18__H.A.MATH.SP.P.FP.L1.19__H.A.MATH.SP.P.FP.L1.2__H.A.MATH.SP.P.FP.L1.20__H.A.MATH.SP.P.FP.L1.3__H.A.MATH.SP.P.FP.L1.4__H.A.MATH.SP.P.FP.L1.5__H.A.MATH.SP.P.FP.L1.6__H.A.MATH.SP.P.FP.L1.7__H.A.MATH.SP.P.FP.L1.8__H.A.MATH.SP.P.FP.L1.9__H.A.MATH.SP.P.FP.L2.1__H.A.MATH.SP.P.FP.L2.2__H.A.MATH.SP.P.FP.L2.3__H.A.MATH.SP.P.FP.L2.4__H.A.MATH.SP.P.FP.L2.5__H.A.MATH.SP.P.FP.L2.6__H.A.MATH.SP.P.FP.L2.7__H.A.MATH.SP.P.FP.L2.8__H.A.MATH.SP.P.FP.L2.9__H.A.MATH.SP.P.FP.L3.1__H.A.MATH.SP.P.FP.L3.10__H.A.MATH.SP.P.FP.L3.11__H.A.MATH.SP.P.FP.L3.2__H.A.MATH.SP.P.FP.L3.3__H.A.MATH.SP.P.FP.L3.4__H.A.MATH.SP.P.FP.L3.5__H.A.MATH.SP.P.FP.L3.6__H.A.MATH.SP.P.FP.L3.7__H.A.MATH.SP.P.FP.L3.8__H.A.MATH.SP.P.FP.L3.9__H.A.MATH.SP.P.FP.L4.1__H.A.MATH.SP.P.FP.L4.10__H.A.MATH.SP.P.FP.L4.2__H.A.MATH.SP.P.FP.L4.3__H.A.MATH.SP.P.FP.L4.4__H.A.MATH.SP.P.FP.L4.5__H.A.MATH.SP.P.FP.L4.6__H.A.MATH.SP.P.FP.L4.7__H.A.MATH.SP.P.FP.L4.8__H.A.MATH.SP.P.FP.L4.9'
	},
	{
		:label => 'H.A.MATH.SP.STL Set Theory and Logic',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.SP.STL.L.L1__H.A.MATH.SP.STL.ST.L1__H.A.MATH.SP.STL.ST.L2'
	}
]

@grade = '&grade=8-9-10-11-12'

@video = '&resource_type=video'

@quiz = '&resource_type=assessment'

@hf.each do |skill|

	puts "h4. #{skill[:label]}"

	puts '||Grades||All||Videos||Assessments||'

	@url = "#{skill[:url]}" + '&license=free'

	print '|All|'

	browser.goto @url
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists?
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @video
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @quiz
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		puts '0|'
	else
		puts browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	print '|8,9,10,11,12|'

	browser.goto @url + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @video + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @quiz + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		puts '0|'
	else
		puts browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end
end