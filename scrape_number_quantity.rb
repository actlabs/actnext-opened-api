require 'watir'

browser = Watir::Browser.new(:chrome)

@hf = [
	{
		:label => 'H.A.MATH.NQ.CC Counting and Cardinality',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.CC.CO.L1.1__H.A.MATH.NQ.CC.CO.L1.2__H.A.MATH.NQ.CC.CO.L1.3__H.A.MATH.NQ.CC.CO.L1.4__H.A.MATH.NQ.CC.CO.L1.5__H.A.MATH.NQ.CC.CO.L1.6__H.A.MATH.NQ.CC.CO.L1.7__H.A.MATH.NQ.CC.CO.L2.1__H.A.MATH.NQ.CC.CO.L2.2__H.A.MATH.NQ.CC.CO.L2.3__H.A.MATH.NQ.CC.CO.L2.4__H.A.MATH.NQ.CC.CO.L2.5__H.A.MATH.NQ.CC.CO.L3.1__H.A.MATH.NQ.CC.CO.L3.2__H.A.MATH.NQ.CC.CO.L3.3__H.A.MATH.NQ.CC.CO.L3.4__H.A.MATH.NQ.CC.CO.L4.1__H.A.MATH.NQ.CC.CO.L4.2__H.A.MATH.NQ.CC.CO.L4.3__H.A.MATH.NQ.CC.CO.L4.4__H.A.MATH.NQ.CC.CPT.L1.1__H.A.MATH.NQ.CC.CPT.L1.2__H.A.MATH.NQ.CC.CPT.L1.3__H.A.MATH.NQ.CC.CPT.L1.4__H.A.MATH.NQ.CC.CPT.L2.1__H.A.MATH.NQ.CC.CPT.L2.2__H.A.MATH.NQ.CC.CPT.L2.3__H.A.MATH.NQ.CC.CPT.L3.1__H.A.MATH.NQ.CC.CPT.L3.2__H.A.MATH.NQ.CC.CPT.L3.3__H.A.MATH.NQ.CC.CPT.L3.4__H.A.MATH.NQ.CC.CS.L1.1__H.A.MATH.NQ.CC.CS.L1.2__H.A.MATH.NQ.CC.CS.L2.1__H.A.MATH.NQ.CC.CS.L2.2__H.A.MATH.NQ.CC.CS.L2.3__H.A.MATH.NQ.CC.CS.L2.4__H.A.MATH.NQ.CC.CS.L2.5__H.A.MATH.NQ.CC.CS.L2.6__H.A.MATH.NQ.CC.CS.L2.7__H.A.MATH.NQ.CC.CS.L3.1__H.A.MATH.NQ.CC.CS.L3.2__H.A.MATH.NQ.CC.CS.L3.3__H.A.MATH.NQ.CC.CS.L3.4__H.A.MATH.NQ.CC.CS.L3.5__H.A.MATH.NQ.CC.CS.L3.6__H.A.MATH.NQ.CC.CS.L4.1__H.A.MATH.NQ.CC.CS.L4.2__H.A.MATH.NQ.CC.CS.L4.3__H.A.MATH.NQ.CC.CS.L4.4__H.A.MATH.NQ.CC.CS.L4.5__H.A.MATH.NQ.CC.CS.L4.6__H.A.MATH.NQ.CC.CS.L4.7__H.A.MATH.NQ.CC.CS.L4.8__H.A.MATH.NQ.CC.CS.L5.1__H.A.MATH.NQ.CC.CS.L5.2__H.A.MATH.NQ.CC.CS.L6.1__H.A.MATH.NQ.CC.CS.L6.2__H.A.MATH.NQ.CC.CT.L2.1__H.A.MATH.NQ.CC.CT.L2.2__H.A.MATH.NQ.CC.NL.L1.1__H.A.MATH.NQ.CC.NL.L1.10__H.A.MATH.NQ.CC.NL.L1.11__H.A.MATH.NQ.CC.NL.L1.12__H.A.MATH.NQ.CC.NL.L1.13__H.A.MATH.NQ.CC.NL.L1.14__H.A.MATH.NQ.CC.NL.L1.15__H.A.MATH.NQ.CC.NL.L1.16__H.A.MATH.NQ.CC.NL.L1.2__H.A.MATH.NQ.CC.NL.L1.3__H.A.MATH.NQ.CC.NL.L1.4__H.A.MATH.NQ.CC.NL.L1.5__H.A.MATH.NQ.CC.NL.L1.6__H.A.MATH.NQ.CC.NL.L1.7__H.A.MATH.NQ.CC.NL.L1.8__H.A.MATH.NQ.CC.NL.L1.9__H.A.MATH.NQ.CC.NL.L2.1__H.A.MATH.NQ.CC.NL.L2.2__H.A.MATH.NQ.CC.NL.L2.3__H.A.MATH.NQ.CC.NL.L2.4__H.A.MATH.NQ.CC.NL.L2.5__H.A.MATH.NQ.CC.NL.L3.1__H.A.MATH.NQ.CC.NL.L3.2__H.A.MATH.NQ.CC.NL.L4.1__H.A.MATH.NQ.CC.NL.L4.2__H.A.MATH.NQ.CC.NL.L4.3__H.A.MATH.NQ.CC.NL.L4.4__H.A.MATH.NQ.CC.PC.L1.1__H.A.MATH.NQ.CC.PC.L1.2'
	},
	{
		:label => 'H.A.MATH.NQ.D Decimals',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.D.COD.L1.1__H.A.MATH.NQ.D.COD.L1.2__H.A.MATH.NQ.D.COD.L1.3__H.A.MATH.NQ.D.COD.L1.4__H.A.MATH.NQ.D.COD.L2.1__H.A.MATH.NQ.D.COD.L2.2__H.A.MATH.NQ.D.COD.L2.3__H.A.MATH.NQ.D.COD.L2.4__H.A.MATH.NQ.D.COD.L2.5__H.A.MATH.NQ.D.COD.L2.6__H.A.MATH.NQ.D.COD.L2.7__H.A.MATH.NQ.D.COD.L2.8__H.A.MATH.NQ.D.COD.L2.9__H.A.MATH.NQ.D.COD.L3.1__H.A.MATH.NQ.D.COD.L3.2__H.A.MATH.NQ.D.COD.L3.3__H.A.MATH.NQ.D.COD.L3.4__H.A.MATH.NQ.D.NRD.L1.1__H.A.MATH.NQ.D.NRD.L1.2__H.A.MATH.NQ.D.NRD.L1.3__H.A.MATH.NQ.D.NRD.L1.4__H.A.MATH.NQ.D.NRD.L1.5__H.A.MATH.NQ.D.NRD.L1.6__H.A.MATH.NQ.D.NRD.L1.7__H.A.MATH.NQ.D.NRD.L2.1__H.A.MATH.NQ.D.NRD.L2.2__H.A.MATH.NQ.D.NRD.L2.3__H.A.MATH.NQ.D.NRD.L2.4__H.A.MATH.NQ.D.NRD.L3.1__H.A.MATH.NQ.D.NRD.L3.2__H.A.MATH.NQ.D.NRD.L3.3__H.A.MATH.NQ.D.NRD.L3.4__H.A.MATH.NQ.D.NRD.L4.1__H.A.MATH.NQ.D.NRD.L4.2__H.A.MATH.NQ.D.NRD.L4.3__H.A.MATH.NQ.D.PVD.L1.1__H.A.MATH.NQ.D.PVD.L1.2__H.A.MATH.NQ.D.PVD.L1.3__H.A.MATH.NQ.D.PVD.L1.4__H.A.MATH.NQ.D.PVD.L1.5__H.A.MATH.NQ.D.PVD.L1.6__H.A.MATH.NQ.D.PVD.L1.7__H.A.MATH.NQ.D.PVD.L1.8__H.A.MATH.NQ.D.PVD.L1.9__H.A.MATH.NQ.D.PVD.L2.1__H.A.MATH.NQ.D.PVD.L2.2__H.A.MATH.NQ.D.PVD.L2.3__H.A.MATH.NQ.D.PVD.L2.4__H.A.MATH.NQ.D.PVD.L2.5__H.A.MATH.NQ.D.PVD.L2.6__H.A.MATH.NQ.D.PVD.L2.7__H.A.MATH.NQ.D.PVD.L3.1__H.A.MATH.NQ.D.PVD.L3.2__H.A.MATH.NQ.D.PVD.L3.3__H.A.MATH.NQ.D.PVD.L3.4__H.A.MATH.NQ.D.PVD.L3.5__H.A.MATH.NQ.D.PVD.L3.6__H.A.MATH.NQ.D.PVD.L4.1__H.A.MATH.NQ.D.PVD.L4.2__H.A.MATH.NQ.D.PVD.L4.3__H.A.MATH.NQ.D.PVD.L4.4__H.A.MATH.NQ.D.PVD.L4.5__H.A.MATH.NQ.D.PVD.L5.1__H.A.MATH.NQ.D.PVD.L5.2__H.A.MATH.NQ.D.PVD.L5.3__H.A.MATH.NQ.D.PVD.L5.4__H.A.MATH.NQ.D.PVD.L6.1__H.A.MATH.NQ.D.PVD.L6.2__H.A.MATH.NQ.D.PVD.L6.3__H.A.MATH.NQ.D.PVD.L6.4__H.A.MATH.NQ.D.PVD.L6.5__H.A.MATH.NQ.D.PVD.L6.6__H.A.MATH.NQ.D.RD.L1.1__H.A.MATH.NQ.D.RD.L1.2__H.A.MATH.NQ.D.RD.L1.3__H.A.MATH.NQ.D.RD.L1.4__H.A.MATH.NQ.D.RD.L2.1'
	},
	{
		:label => 'H.A.MATH.NQ.FC Fraction Concepts',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.FC.COF.L1.1__H.A.MATH.NQ.FC.COF.L1.2__H.A.MATH.NQ.FC.COF.L1.3__H.A.MATH.NQ.FC.COF.L1.4__H.A.MATH.NQ.FC.COF.L1.5__H.A.MATH.NQ.FC.COF.L2.1__H.A.MATH.NQ.FC.COF.L2.2__H.A.MATH.NQ.FC.COF.L2.3__H.A.MATH.NQ.FC.COF.L2.4__H.A.MATH.NQ.FC.COF.L3.1__H.A.MATH.NQ.FC.COF.L3.2__H.A.MATH.NQ.FC.EQV.L1.1__H.A.MATH.NQ.FC.EQV.L1.2__H.A.MATH.NQ.FC.EQV.L2.1__H.A.MATH.NQ.FC.EQV.L3.1__H.A.MATH.NQ.FC.EQV.L3.2__H.A.MATH.NQ.FC.EQV.L3.3__H.A.MATH.NQ.FC.EQV.L3.4__H.A.MATH.NQ.FC.EQV.L4.1__H.A.MATH.NQ.FC.EQV.L4.2__H.A.MATH.NQ.FC.EQV.L4.3__H.A.MATH.NQ.FC.EQV.L4.4__H.A.MATH.NQ.FC.EQV.L4.5__H.A.MATH.NQ.FC.NL.L1.1__H.A.MATH.NQ.FC.NL.L1.10__H.A.MATH.NQ.FC.NL.L1.11__H.A.MATH.NQ.FC.NL.L1.12__H.A.MATH.NQ.FC.NL.L1.13__H.A.MATH.NQ.FC.NL.L1.14__H.A.MATH.NQ.FC.NL.L1.15__H.A.MATH.NQ.FC.NL.L1.16__H.A.MATH.NQ.FC.NL.L1.2__H.A.MATH.NQ.FC.NL.L1.3__H.A.MATH.NQ.FC.NL.L1.4__H.A.MATH.NQ.FC.NL.L1.5__H.A.MATH.NQ.FC.NL.L1.6__H.A.MATH.NQ.FC.NL.L1.7__H.A.MATH.NQ.FC.NL.L1.8__H.A.MATH.NQ.FC.NL.L1.9__H.A.MATH.NQ.FC.NL.L2.1__H.A.MATH.NQ.FC.NL.L3.1__H.A.MATH.NQ.FC.PW.L1.1__H.A.MATH.NQ.FC.PW.L1.2__H.A.MATH.NQ.FC.PW.L1.3__H.A.MATH.NQ.FC.PW.L1.4__H.A.MATH.NQ.FC.PW.L1.5__H.A.MATH.NQ.FC.PW.L1.6__H.A.MATH.NQ.FC.PW.L1.7__H.A.MATH.NQ.FC.PW.L2.1__H.A.MATH.NQ.FC.PW.L2.2__H.A.MATH.NQ.FC.PW.L2.3__H.A.MATH.NQ.FC.PW.L2.4__H.A.MATH.NQ.FC.PW.L2.5__H.A.MATH.NQ.FC.PW.L2.6__H.A.MATH.NQ.FC.PW.L2.7__H.A.MATH.NQ.FC.PW.L2.8__H.A.MATH.NQ.FC.PW.L2.9__H.A.MATH.NQ.FC.PW.L3.1__H.A.MATH.NQ.FC.PW.L3.2__H.A.MATH.NQ.FC.PW.L3.3__H.A.MATH.NQ.FC.PW.L3.4__H.A.MATH.NQ.FC.PW.L3.5__H.A.MATH.NQ.FC.PW.L3.6__H.A.MATH.NQ.FC.PW.L4.1__H.A.MATH.NQ.FC.PW.L4.2__H.A.MATH.NQ.FC.PW.L4.3__H.A.MATH.NQ.FC.PW.L4.4__H.A.MATH.NQ.FC.PW.L4.5__H.A.MATH.NQ.FC.PW.L4.6__H.A.MATH.NQ.FC.PW.L4.7__H.A.MATH.NQ.FC.PW.L5.1__H.A.MATH.NQ.FC.PW.L5.2__H.A.MATH.NQ.FC.PW.L5.3__H.A.MATH.NQ.FC.PW.L5.4__H.A.MATH.NQ.FC.PW.L5.5__H.A.MATH.NQ.FC.PW.L5.6__H.A.MATH.NQ.FC.PW.L5.7__H.A.MATH.NQ.FC.PW.L5.8__H.A.MATH.NQ.FC.PW.L5.9__H.A.MATH.NQ.FC.PW.L6.1__H.A.MATH.NQ.FC.PW.L6.2__H.A.MATH.NQ.FC.PW.L6.3__H.A.MATH.NQ.FC.PW.L6.4__H.A.MATH.NQ.FC.PW.L6.5__H.A.MATH.NQ.FC.PW.L6.6__H.A.MATH.NQ.FC.PW.L6.7__H.A.MATH.NQ.FC.PW.L6.8'
	},
	{
		:label => 'H.A.MATH.NQ.QE Quantity Estimation',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.QE.QE.L1.1__H.A.MATH.NQ.QE.QE.L1.2__H.A.MATH.NQ.QE.QE.L1.3__H.A.MATH.NQ.QE.QE.L2.1__H.A.MATH.NQ.QE.QE.L2.2__H.A.MATH.NQ.QE.QE.L3.1__H.A.MATH.NQ.QE.QE.L3.2'
	},
	{
		:label => 'H.A.MATH.NQ.RCN Real and Complex Numbers',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.RCN.CN.L1.1__H.A.MATH.NQ.RCN.CN.L1.2__H.A.MATH.NQ.RCN.CN.L1.3__H.A.MATH.NQ.RCN.CN.L2.1__H.A.MATH.NQ.RCN.CN.L2.2__H.A.MATH.NQ.RCN.CN.L2.3__H.A.MATH.NQ.RCN.E.L1.1__H.A.MATH.NQ.RCN.E.L1.2__H.A.MATH.NQ.RCN.E.L1.3__H.A.MATH.NQ.RCN.E.L1.4__H.A.MATH.NQ.RCN.E.L1.5__H.A.MATH.NQ.RCN.E.L1.6__H.A.MATH.NQ.RCN.E.L2.1__H.A.MATH.NQ.RCN.E.L2.2__H.A.MATH.NQ.RCN.E.L2.3__H.A.MATH.NQ.RCN.E.L2.4__H.A.MATH.NQ.RCN.E.L2.5__H.A.MATH.NQ.RCN.E.L2.6__H.A.MATH.NQ.RCN.E.L2.7__H.A.MATH.NQ.RCN.E.L2.8__H.A.MATH.NQ.RCN.E.L2.9__H.A.MATH.NQ.RCN.RIN.L1.1__H.A.MATH.NQ.RCN.RIN.L1.10__H.A.MATH.NQ.RCN.RIN.L1.2__H.A.MATH.NQ.RCN.RIN.L1.3__H.A.MATH.NQ.RCN.RIN.L1.4__H.A.MATH.NQ.RCN.RIN.L1.5__H.A.MATH.NQ.RCN.RIN.L1.6__H.A.MATH.NQ.RCN.RIN.L1.7__H.A.MATH.NQ.RCN.RIN.L1.8__H.A.MATH.NQ.RCN.RIN.L1.9__H.A.MATH.NQ.RCN.RIN.L2.1__H.A.MATH.NQ.RCN.RIN.L2.2__H.A.MATH.NQ.RCN.RIN.L2.3__H.A.MATH.NQ.RCN.RIN.L2.4'
	},
	{
		:label => 'H.A.MATH.NQ.SN Signed Numbers',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.SN.SI.L1.1__H.A.MATH.NQ.SN.SI.L1.2__H.A.MATH.NQ.SN.SI.L1.3__H.A.MATH.NQ.SN.SI.L1.4__H.A.MATH.NQ.SN.SI.L1.5__H.A.MATH.NQ.SN.SI.L1.6__H.A.MATH.NQ.SN.SI.L1.7__H.A.MATH.NQ.SN.SI.L1.8__H.A.MATH.NQ.SN.SI.L1.9__H.A.MATH.NQ.SN.SI.L2.1__H.A.MATH.NQ.SN.SI.L2.2__H.A.MATH.NQ.SN.SI.L2.3__H.A.MATH.NQ.SN.SI.L3.1__H.A.MATH.NQ.SN.SI.L3.2__H.A.MATH.NQ.SN.SI.L3.3__H.A.MATH.NQ.SN.SI.L3.4__H.A.MATH.NQ.SN.SRN.L1.1__H.A.MATH.NQ.SN.SRN.L1.2__H.A.MATH.NQ.SN.SRN.L2.1__H.A.MATH.NQ.SN.SRN.L2.2__H.A.MATH.NQ.SN.SRN.L3.1__H.A.MATH.NQ.SN.SRN.L3.2__H.A.MATH.NQ.SN.SRN.L3.3__H.A.MATH.NQ.SN.SRN.L3.4__H.A.MATH.NQ.SN.SRN.L3.5'
	},
	{
		:label => 'H.A.MATH.NQ.VM Vectors and Matrices',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.VM.M.L1.1__H.A.MATH.NQ.VM.M.L1.2__H.A.MATH.NQ.VM.M.L1.3__H.A.MATH.NQ.VM.M.L2.1__H.A.MATH.NQ.VM.M.L2.2__H.A.MATH.NQ.VM.M.L2.3__H.A.MATH.NQ.VM.M.L2.4__H.A.MATH.NQ.VM.M.L2.5__H.A.MATH.NQ.VM.V.L1.1__H.A.MATH.NQ.VM.V.L1.2__H.A.MATH.NQ.VM.V.L1.3__H.A.MATH.NQ.VM.V.L2.1__H.A.MATH.NQ.VM.V.L2.2__H.A.MATH.NQ.VM.V.L2.3__H.A.MATH.NQ.VM.V.L2.4'
	},
	{
		:label => 'H.A.MATH.NQ.WN Whole Numbers',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.NQ.WN.COWN.L1.1__H.A.MATH.NQ.WN.COWN.L1.2__H.A.MATH.NQ.WN.COWN.L1.3__H.A.MATH.NQ.WN.COWN.L1.4__H.A.MATH.NQ.WN.COWN.L1.5__H.A.MATH.NQ.WN.COWN.L1.6__H.A.MATH.NQ.WN.COWN.L1.7__H.A.MATH.NQ.WN.COWN.L1.8__H.A.MATH.NQ.WN.COWN.L2.1__H.A.MATH.NQ.WN.COWN.L2.2__H.A.MATH.NQ.WN.COWN.L2.3__H.A.MATH.NQ.WN.COWN.L2.4__H.A.MATH.NQ.WN.COWN.L2.5__H.A.MATH.NQ.WN.COWN.L2.6__H.A.MATH.NQ.WN.COWN.L3.1__H.A.MATH.NQ.WN.COWN.L3.2__H.A.MATH.NQ.WN.COWN.L3.3__H.A.MATH.NQ.WN.COWN.L3.4__H.A.MATH.NQ.WN.NRWN.L1.1__H.A.MATH.NQ.WN.NRWN.L1.2__H.A.MATH.NQ.WN.NRWN.L1.3__H.A.MATH.NQ.WN.NRWN.L1.4__H.A.MATH.NQ.WN.NRWN.L1.5__H.A.MATH.NQ.WN.NRWN.L2.1__H.A.MATH.NQ.WN.NRWN.L2.2__H.A.MATH.NQ.WN.NRWN.L2.3__H.A.MATH.NQ.WN.NRWN.L2.4__H.A.MATH.NQ.WN.NRWN.L2.5__H.A.MATH.NQ.WN.NRWN.L2.6__H.A.MATH.NQ.WN.NRWN.L3.1__H.A.MATH.NQ.WN.NRWN.L3.2__H.A.MATH.NQ.WN.NRWN.L3.3__H.A.MATH.NQ.WN.NRWN.L3.4__H.A.MATH.NQ.WN.NRWN.L3.5__H.A.MATH.NQ.WN.NRWN.L3.6__H.A.MATH.NQ.WN.NRWN.L4.1__H.A.MATH.NQ.WN.NRWN.L4.2__H.A.MATH.NQ.WN.PVWN.L1.1__H.A.MATH.NQ.WN.PVWN.L1.2__H.A.MATH.NQ.WN.PVWN.L1.3__H.A.MATH.NQ.WN.PVWN.L1.4__H.A.MATH.NQ.WN.PVWN.L1.5__H.A.MATH.NQ.WN.PVWN.L1.6__H.A.MATH.NQ.WN.PVWN.L1.7__H.A.MATH.NQ.WN.PVWN.L1.8__H.A.MATH.NQ.WN.PVWN.L1.9__H.A.MATH.NQ.WN.PVWN.L2.1__H.A.MATH.NQ.WN.PVWN.L2.10__H.A.MATH.NQ.WN.PVWN.L2.11__H.A.MATH.NQ.WN.PVWN.L2.12__H.A.MATH.NQ.WN.PVWN.L2.13__H.A.MATH.NQ.WN.PVWN.L2.14__H.A.MATH.NQ.WN.PVWN.L2.15__H.A.MATH.NQ.WN.PVWN.L2.16__H.A.MATH.NQ.WN.PVWN.L2.2__H.A.MATH.NQ.WN.PVWN.L2.3__H.A.MATH.NQ.WN.PVWN.L2.4__H.A.MATH.NQ.WN.PVWN.L2.5__H.A.MATH.NQ.WN.PVWN.L2.6__H.A.MATH.NQ.WN.PVWN.L2.7__H.A.MATH.NQ.WN.PVWN.L2.8__H.A.MATH.NQ.WN.PVWN.L2.9__H.A.MATH.NQ.WN.PVWN.L3.1__H.A.MATH.NQ.WN.PVWN.L3.10__H.A.MATH.NQ.WN.PVWN.L3.11__H.A.MATH.NQ.WN.PVWN.L3.12__H.A.MATH.NQ.WN.PVWN.L3.13__H.A.MATH.NQ.WN.PVWN.L3.14__H.A.MATH.NQ.WN.PVWN.L3.15__H.A.MATH.NQ.WN.PVWN.L3.2__H.A.MATH.NQ.WN.PVWN.L3.3__H.A.MATH.NQ.WN.PVWN.L3.4__H.A.MATH.NQ.WN.PVWN.L3.5__H.A.MATH.NQ.WN.PVWN.L3.6__H.A.MATH.NQ.WN.PVWN.L3.7__H.A.MATH.NQ.WN.PVWN.L3.8__H.A.MATH.NQ.WN.PVWN.L3.9__H.A.MATH.NQ.WN.PVWN.L4.1__H.A.MATH.NQ.WN.PVWN.L4.10__H.A.MATH.NQ.WN.PVWN.L4.11__H.A.MATH.NQ.WN.PVWN.L4.2__H.A.MATH.NQ.WN.PVWN.L4.3__H.A.MATH.NQ.WN.PVWN.L4.4__H.A.MATH.NQ.WN.PVWN.L4.5__H.A.MATH.NQ.WN.PVWN.L4.6__H.A.MATH.NQ.WN.PVWN.L4.7__H.A.MATH.NQ.WN.PVWN.L4.8__H.A.MATH.NQ.WN.PVWN.L4.9__H.A.MATH.NQ.WN.PVWN.L5.1__H.A.MATH.NQ.WN.PVWN.L5.10__H.A.MATH.NQ.WN.PVWN.L5.11__H.A.MATH.NQ.WN.PVWN.L5.12__H.A.MATH.NQ.WN.PVWN.L5.13__H.A.MATH.NQ.WN.PVWN.L5.14__H.A.MATH.NQ.WN.PVWN.L5.15__H.A.MATH.NQ.WN.PVWN.L5.16__H.A.MATH.NQ.WN.PVWN.L5.17__H.A.MATH.NQ.WN.PVWN.L5.2__H.A.MATH.NQ.WN.PVWN.L5.3__H.A.MATH.NQ.WN.PVWN.L5.4__H.A.MATH.NQ.WN.PVWN.L5.5__H.A.MATH.NQ.WN.PVWN.L5.6__H.A.MATH.NQ.WN.PVWN.L5.7__H.A.MATH.NQ.WN.PVWN.L5.8__H.A.MATH.NQ.WN.PVWN.L5.9__H.A.MATH.NQ.WN.PVWN.L6.1__H.A.MATH.NQ.WN.PVWN.L6.2__H.A.MATH.NQ.WN.PVWN.L6.3__H.A.MATH.NQ.WN.PVWN.L6.4__H.A.MATH.NQ.WN.PVWN.L6.5__H.A.MATH.NQ.WN.PVWN.L6.6__H.A.MATH.NQ.WN.PVWN.L6.7__H.A.MATH.NQ.WN.PVWN.L7.1__H.A.MATH.NQ.WN.PVWN.L7.2__H.A.MATH.NQ.WN.PVWN.L8.1__H.A.MATH.NQ.WN.PVWN.L8.2__H.A.MATH.NQ.WN.PVWN.L8.3__H.A.MATH.NQ.WN.PVWN.L8.4__H.A.MATH.NQ.WN.PVWN.L8.5__H.A.MATH.NQ.WN.PVWN.L8.6__H.A.MATH.NQ.WN.PVWN.L8.7__H.A.MATH.NQ.WN.RWN.L1.1__H.A.MATH.NQ.WN.RWN.L1.2__H.A.MATH.NQ.WN.RWN.L1.3__H.A.MATH.NQ.WN.RWN.L1.4__H.A.MATH.NQ.WN.RWN.L2.1__H.A.MATH.NQ.WN.RWN.L2.2__H.A.MATH.NQ.WN.RWN.L2.3__H.A.MATH.NQ.WN.RWN.L3.1'
	},
]

@grade = '&grade=8-9-10-11-12'

@video = '&resource_type=video'

@quiz = '&resource_type=assessment'

@hf.each do |skill|

	puts "h4. #{skill[:label]}"

	puts '||Grades||All||Videos||Assessments||'

	@url = "#{skill[:url]}" + '&license=free'

	print '|All|'

	browser.goto @url
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists?
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @video
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @quiz
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		puts '0|'
	else
		puts browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	print '|8,9,10,11,12|'

	browser.goto @url + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @video + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @quiz + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		puts '0|'
	else
		puts browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end
end