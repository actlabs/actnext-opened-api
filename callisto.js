'use strict';

var http = require('http');
var qs = require('querystring');
var url = require('url');
var ip = require('ip');
var Q = require('q');
var _ = require('lodash');
var request = require('request');

var token = null;


function queryAssessmentEvents(req, res, id, page) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, 
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		var p = (page) ? page : 1; 

		request({
			method: 'GET',
			url: 'https://www.opencallisto.org/assessment_events.json?actor_id=' + encodeURI('https://www.opened.com/user/' + id + '&page=' + p),
			headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
		},
		function (error, response, body) {
			console.log('---------------------------- user ' + id + ' body ----------------------------');
			console.log(body);
			return res.end(body)
		});
	});
}



function queryAssessmentItemEvents(req, res, id, page) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, 
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		var p = (page) ? page : 1; 

		request({
			method: 'GET',
			url: 'https://www.opencallisto.org/assessment_item_events.json?actor_id=' + encodeURI('https://www.opened.com/user/' + id + '&page=' + p),
			headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
		},
		function (error, response, body) {
			console.log('---------------------------- user ' + id + ' body ----------------------------');
			console.log(body);
			return res.end(body)
		});
	});
}

function queryMediaEvents(req, res, id) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		request({
				method: 'GET',
			url: 'https://www.opencallisto.org/media_events.json?actor_id=' + encodeURI('https://www.opened.com/user/' + id),
				headers: {
					'Content-Type': 'application/json',
					'uid': uid,
					'client': client,
					'access-token': access_token
				}
			},
			function (error, response, body) {
				return res.end(body)
			});
	});
}


function route(req, res) {

	// extremely simple url based api
	var inUrl = url.parse(req.url, true);
	var command = inUrl.pathname.substr(1);

	if (command) {

		// jsonp
		if (inUrl.query.callback === undefined || inUrl.query.callback === '') {
			inUrl.query.callback = 'callback';
		}

		switch (command) {
			case 'queryAssessmentEvents': {
				if (inUrl.query.id) {
					return queryAssessmentEvents(req, res, inUrl.query.id, inUrl.query.page);
				}
				break;
			}
			case 'queryAssessmentItemEvents': {
				if (inUrl.query.id) {
					return queryAssessmentItemEvents(req, res, inUrl.query.id, inUrl.query.page);
				}
				break;
			}
			case 'queryMediaEvents':
				{
					if (inUrl.query.id) {
						return queryMediaEvents(req, res, inUrl.query.id);
					}
					break;
				}
			default: {
				break;
			}
		}
	}

	res.end('{}');
}

//contentSearch('World War II');

var d = require('domain').create();

d.on('error', function(e) {
	console.log(e);
});

d.run(function() {
	http.createServer(function (req, res) {
		res.writeHead(200, {'Content-Type': 'application/json'});
		route(req, res);
	}).listen(4444, '0.0.0.0');
});

console.log('Server running at http://' + ip.address() + ':4444/');