//node convertCsvToJson.js mathv11.csv
var csv = require("fast-csv"),
    async = require('async');

function convert(filename,result,next) {
	var count=1;
	var items = {};
	var users = {};
  var answers = {};

	csv
	.fromPath(filename)
	.on("data", function(data){
		if(count > 1) {
      var resourceString = data[0];
      var resourceArray = resourceString.split('/');
      var resource = resourceArray[4];
      items[resource] = '';

      var userString = data[1];
      var userArray = userString.split('/');
      var user = userArray[4];
      users[user] = '';

      answers[user+':'+resource] = data[2];
 	
		}
		count++;
	})
	.on("end", function(){
        //header
        var header = ','
        for (var item in items) {
          header += item + ','
        }
        console.log(header)
        for (var user in users) {
          var output = user + ',';
          for (var item in items) {
            var key = user + ':' + item
            if(answers.hasOwnProperty(key)){
              output += answers[key] + ','
            } else {
              output += ' ,'
            }
          }
          console.log(output)
        }
        return next();
    })	
} 

var result = {};
result.data = [];
async.series([
  function(next) {
  	convert(process.argv[2],result,next);
  },
  function(next) {
  	//console.log(JSON.stringify(result));
  	return next();
  }
], function(err) {});