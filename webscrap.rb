require 'watir'

browser = Watir::Browser.new(:chrome)

@hf = [
	{
		:label => 'H.A.MATH.GM.3DF 3-Dimensional Figures',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.GM.3DF.3DFP.L1.1__H.A.MATH.GM.3DF.3DFP.L1.2__H.A.MATH.GM.3DF.3DFP.L1.3__H.A.MATH.GM.3DF.3DFP.L1.4__H.A.MATH.GM.3DF.3DFP.L1.5__H.A.MATH.GM.3DF.3DFP.L1.6__H.A.MATH.GM.3DF.3DFP.L1.7__H.A.MATH.GM.3DF.3DFP.L1.8__H.A.MATH.GM.3DF.3DFP.L2.1__H.A.MATH.GM.3DF.3DFP.L2.2__H.A.MATH.GM.3DF.3DFP.L2.3__H.A.MATH.GM.3DF.3DFP.L2.4__H.A.MATH.GM.3DF.3DFP.L2.5__H.A.MATH.GM.3DF.3DFP.L2.6__H.A.MATH.GM.3DF.3DFP.L2.7__H.A.MATH.GM.3DF.3DFP.L2.8__H.A.MATH.GM.3DF.C3DS.L1.1__H.A.MATH.GM.3DF.C3DS.L1.2__H.A.MATH.GM.3DF.C3DS.L1.3__H.A.MATH.GM.3DF.C3DS.L1.4__H.A.MATH.GM.3DF.C3DS.L1.5__H.A.MATH.GM.3DF.C3DS.L2.1__H.A.MATH.GM.3DF.C3DS.L2.2__H.A.MATH.GM.3DF.C3DS.L2.3__H.A.MATH.GM.3DF.C3DS.L2.4__H.A.MATH.GM.3DF.C3DS.L3.1__H.A.MATH.GM.3DF.C3DS.L3.2__H.A.MATH.GM.3DF.C3DS.L3.3__H.A.MATH.GM.3DF.C3DS.L4.1__H.A.MATH.GM.3DF.C3DS.L4.2__H.A.MATH.GM.3DF.CCS.L1.1__H.A.MATH.GM.3DF.CCS.L1.10__H.A.MATH.GM.3DF.CCS.L1.11__H.A.MATH.GM.3DF.CCS.L1.2__H.A.MATH.GM.3DF.CCS.L1.3__H.A.MATH.GM.3DF.CCS.L1.4__H.A.MATH.GM.3DF.CCS.L1.5__H.A.MATH.GM.3DF.CCS.L1.6__H.A.MATH.GM.3DF.CCS.L1.7__H.A.MATH.GM.3DF.CCS.L1.8__H.A.MATH.GM.3DF.CCS.L1.9__H.A.MATH.GM.3DF.CCS.L2.1__H.A.MATH.GM.3DF.CCS.L2.10__H.A.MATH.GM.3DF.CCS.L2.11__H.A.MATH.GM.3DF.CCS.L2.2__H.A.MATH.GM.3DF.CCS.L2.3__H.A.MATH.GM.3DF.CCS.L2.4__H.A.MATH.GM.3DF.CCS.L2.5__H.A.MATH.GM.3DF.CCS.L2.6__H.A.MATH.GM.3DF.CCS.L2.7__H.A.MATH.GM.3DF.CCS.L2.8__H.A.MATH.GM.3DF.CCS.L2.9__H.A.MATH.GM.3DF.FV.L1.1__H.A.MATH.GM.3DF.FV.L1.10__H.A.MATH.GM.3DF.FV.L1.11__H.A.MATH.GM.3DF.FV.L1.12__H.A.MATH.GM.3DF.FV.L1.13__H.A.MATH.GM.3DF.FV.L1.14__H.A.MATH.GM.3DF.FV.L1.2__H.A.MATH.GM.3DF.FV.L1.3__H.A.MATH.GM.3DF.FV.L1.4__H.A.MATH.GM.3DF.FV.L1.5__H.A.MATH.GM.3DF.FV.L1.6__H.A.MATH.GM.3DF.FV.L1.7__H.A.MATH.GM.3DF.FV.L1.8__H.A.MATH.GM.3DF.FV.L1.9__H.A.MATH.GM.3DF.FV.L2.1__H.A.MATH.GM.3DF.FV.L2.10__H.A.MATH.GM.3DF.FV.L2.11__H.A.MATH.GM.3DF.FV.L2.2__H.A.MATH.GM.3DF.FV.L2.3__H.A.MATH.GM.3DF.FV.L2.4__H.A.MATH.GM.3DF.FV.L2.5__H.A.MATH.GM.3DF.FV.L2.6__H.A.MATH.GM.3DF.FV.L2.7__H.A.MATH.GM.3DF.FV.L2.8__H.A.MATH.GM.3DF.FV.L2.9__H.A.MATH.GM.3DF.FV.L3.1__H.A.MATH.GM.3DF.FV.L3.2__H.A.MATH.GM.3DF.FV.L4.1__H.A.MATH.GM.3DF.FV.L4.2__H.A.MATH.GM.3DF.FV.L4.3__H.A.MATH.GM.3DF.MW.L1.1__H.A.MATH.GM.3DF.MW.L1.2__H.A.MATH.GM.3DF.MW.L1.3__H.A.MATH.GM.3DF.MW.L1.4__H.A.MATH.GM.3DF.MW.L1.5__H.A.MATH.GM.3DF.MW.L1.6__H.A.MATH.GM.3DF.MW.L1.7__H.A.MATH.GM.3DF.MW.L2.1__H.A.MATH.GM.3DF.MW.L2.2__H.A.MATH.GM.3DF.MW.L2.3__H.A.MATH.GM.3DF.MW.L2.4__H.A.MATH.GM.3DF.MW.L2.5__H.A.MATH.GM.3DF.MW.L2.6__H.A.MATH.GM.3DF.MW.L3.1__H.A.MATH.GM.3DF.MW.L3.2__H.A.MATH.GM.3DF.MW.L4.1__H.A.MATH.GM.3DF.MW.L4.2__H.A.MATH.GM.3DF.MW.L4.3__H.A.MATH.GM.3DF.MW.L4.4__H.A.MATH.GM.3DF.MW.L4.5__H.A.MATH.GM.3DF.MW.L4.6__H.A.MATH.GM.3DF.MW.L4.7__H.A.MATH.GM.3DF.MW.L5.1__H.A.MATH.GM.3DF.MW.L5.2__H.A.MATH.GM.3DF.MW.L5.3__H.A.MATH.GM.3DF.MW.L5.4__H.A.MATH.GM.3DF.MW.L5.5__H.A.MATH.GM.3DF.MW.L5.6__H.A.MATH.GM.3DF.MW.L5.7__H.A.MATH.GM.3DF.PV.L1.1__H.A.MATH.GM.3DF.PV.L1.2__H.A.MATH.GM.3DF.PV.L1.3__H.A.MATH.GM.3DF.PV.L1.4__H.A.MATH.GM.3DF.PV.L1.5__H.A.MATH.GM.3DF.PV.L1.6__H.A.MATH.GM.3DF.PV.L1.7__H.A.MATH.GM.3DF.PV.L1.8__H.A.MATH.GM.3DF.PV.L1.9__H.A.MATH.GM.3DF.PV.L2.1__H.A.MATH.GM.3DF.PV.L2.2__H.A.MATH.GM.3DF.PV.L2.3__H.A.MATH.GM.3DF.PV.L2.4__H.A.MATH.GM.3DF.PV.L2.5__H.A.MATH.GM.3DF.PV.L3.1__H.A.MATH.GM.3DF.PV.L3.10__H.A.MATH.GM.3DF.PV.L3.11__H.A.MATH.GM.3DF.PV.L3.12__H.A.MATH.GM.3DF.PV.L3.13__H.A.MATH.GM.3DF.PV.L3.14__H.A.MATH.GM.3DF.PV.L3.15__H.A.MATH.GM.3DF.PV.L3.16__H.A.MATH.GM.3DF.PV.L3.17__H.A.MATH.GM.3DF.PV.L3.18__H.A.MATH.GM.3DF.PV.L3.19__H.A.MATH.GM.3DF.PV.L3.2__H.A.MATH.GM.3DF.PV.L3.3__H.A.MATH.GM.3DF.PV.L3.4__H.A.MATH.GM.3DF.PV.L3.5__H.A.MATH.GM.3DF.PV.L3.6__H.A.MATH.GM.3DF.PV.L3.7__H.A.MATH.GM.3DF.PV.L3.8__H.A.MATH.GM.3DF.PV.L3.9__H.A.MATH.GM.3DF.PV.L4.1__H.A.MATH.GM.3DF.PV.L4.10__H.A.MATH.GM.3DF.PV.L4.11__H.A.MATH.GM.3DF.PV.L4.12__H.A.MATH.GM.3DF.PV.L4.13__H.A.MATH.GM.3DF.PV.L4.14__H.A.MATH.GM.3DF.PV.L4.15__H.A.MATH.GM.3DF.PV.L4.2__H.A.MATH.GM.3DF.PV.L4.3__H.A.MATH.GM.3DF.PV.L4.4__H.A.MATH.GM.3DF.PV.L4.5__H.A.MATH.GM.3DF.PV.L4.6__H.A.MATH.GM.3DF.PV.L4.7__H.A.MATH.GM.3DF.PV.L4.8__H.A.MATH.GM.3DF.PV.L4.9__H.A.MATH.GM.3DF.PV.L5.1__H.A.MATH.GM.3DF.PV.L5.2__H.A.MATH.GM.3DF.PV.L5.3__H.A.MATH.GM.3DF.PV.L5.4__H.A.MATH.GM.3DF.PV.L5.5__H.A.MATH.GM.3DF.PV.L6.1__H.A.MATH.GM.3DF.PV.L6.2__H.A.MATH.GM.3DF.PV.L6.3__H.A.MATH.GM.3DF.PV.L6.4__H.A.MATH.GM.3DF.PV.L6.5__H.A.MATH.GM.3DF.SA.L1.1__H.A.MATH.GM.3DF.SA.L1.10__H.A.MATH.GM.3DF.SA.L1.11__H.A.MATH.GM.3DF.SA.L1.12__H.A.MATH.GM.3DF.SA.L1.13__H.A.MATH.GM.3DF.SA.L1.2__H.A.MATH.GM.3DF.SA.L1.3__H.A.MATH.GM.3DF.SA.L1.4__H.A.MATH.GM.3DF.SA.L1.5__H.A.MATH.GM.3DF.SA.L1.6__H.A.MATH.GM.3DF.SA.L1.7__H.A.MATH.GM.3DF.SA.L1.8__H.A.MATH.GM.3DF.SA.L1.9__H.A.MATH.GM.3DF.SA.L2.1__H.A.MATH.GM.3DF.SA.L2.2__H.A.MATH.GM.3DF.SA.L2.3__H.A.MATH.GM.3DF.SA.L2.4__H.A.MATH.GM.3DF.SA.L3.1__H.A.MATH.GM.3DF.SA.L3.2'
	},
	{
		:label => 'H.A.MATH.GM.CST Congruence, Similarity, and Transformations',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.GM.CST.C.L1.1__H.A.MATH.GM.CST.C.L1.2__H.A.MATH.GM.CST.C.L1.3__H.A.MATH.GM.CST.C.L1.4__H.A.MATH.GM.CST.C.L1.5__H.A.MATH.GM.CST.C.L1.6__H.A.MATH.GM.CST.C.L1.7__H.A.MATH.GM.CST.C.L2.1__H.A.MATH.GM.CST.C.L2.2__H.A.MATH.GM.CST.C.L2.3__H.A.MATH.GM.CST.S.L1.1__H.A.MATH.GM.CST.S.L1.10__H.A.MATH.GM.CST.S.L1.11__H.A.MATH.GM.CST.S.L1.12__H.A.MATH.GM.CST.S.L1.13__H.A.MATH.GM.CST.S.L1.14__H.A.MATH.GM.CST.S.L1.2__H.A.MATH.GM.CST.S.L1.3__H.A.MATH.GM.CST.S.L1.4__H.A.MATH.GM.CST.S.L1.5__H.A.MATH.GM.CST.S.L1.6__H.A.MATH.GM.CST.S.L1.7__H.A.MATH.GM.CST.S.L1.8__H.A.MATH.GM.CST.S.L1.9__H.A.MATH.GM.CST.S.L2.1__H.A.MATH.GM.CST.S.L2.2__H.A.MATH.GM.CST.S.L2.3__H.A.MATH.GM.CST.SD.L1.1__H.A.MATH.GM.CST.SD.L1.2__H.A.MATH.GM.CST.SD.L1.3__H.A.MATH.GM.CST.SD.L1.4__H.A.MATH.GM.CST.SD.L1.5__H.A.MATH.GM.CST.SD.L2.1__H.A.MATH.GM.CST.SD.L2.2__H.A.MATH.GM.CST.T.L1.1__H.A.MATH.GM.CST.T.L1.10__H.A.MATH.GM.CST.T.L1.11__H.A.MATH.GM.CST.T.L1.12__H.A.MATH.GM.CST.T.L1.13__H.A.MATH.GM.CST.T.L1.2__H.A.MATH.GM.CST.T.L1.3__H.A.MATH.GM.CST.T.L1.4__H.A.MATH.GM.CST.T.L1.5__H.A.MATH.GM.CST.T.L1.6__H.A.MATH.GM.CST.T.L1.7__H.A.MATH.GM.CST.T.L1.8__H.A.MATH.GM.CST.T.L1.9__H.A.MATH.GM.CST.T.L2.1__H.A.MATH.GM.CST.T.L2.2__H.A.MATH.GM.CST.T.L2.3__H.A.MATH.GM.CST.T.L2.4'
	}
]

# @url = 'https://www.opened.com/search?standard=H.A.MATH.GM.CST.C.L1.1__H.A.MATH.GM.CST.C.L1.2__H.A.MATH.GM.CST.C.L1.3__H.A.MATH.GM.CST.C.L1.4__H.A.MATH.GM.CST.C.L1.5__H.A.MATH.GM.CST.C.L1.6__H.A.MATH.GM.CST.C.L1.7__H.A.MATH.GM.CST.C.L2.1__H.A.MATH.GM.CST.C.L2.2__H.A.MATH.GM.CST.C.L2.3__H.A.MATH.GM.CST.S.L1.1__H.A.MATH.GM.CST.S.L1.10__H.A.MATH.GM.CST.S.L1.11__H.A.MATH.GM.CST.S.L1.12__H.A.MATH.GM.CST.S.L1.13__H.A.MATH.GM.CST.S.L1.14__H.A.MATH.GM.CST.S.L1.2__H.A.MATH.GM.CST.S.L1.3__H.A.MATH.GM.CST.S.L1.4__H.A.MATH.GM.CST.S.L1.5__H.A.MATH.GM.CST.S.L1.6__H.A.MATH.GM.CST.S.L1.7__H.A.MATH.GM.CST.S.L1.8__H.A.MATH.GM.CST.S.L1.9__H.A.MATH.GM.CST.S.L2.1__H.A.MATH.GM.CST.S.L2.2__H.A.MATH.GM.CST.S.L2.3__H.A.MATH.GM.CST.SD.L1.1__H.A.MATH.GM.CST.SD.L1.2__H.A.MATH.GM.CST.SD.L1.3__H.A.MATH.GM.CST.SD.L1.4__H.A.MATH.GM.CST.SD.L1.5__H.A.MATH.GM.CST.SD.L2.1__H.A.MATH.GM.CST.SD.L2.2__H.A.MATH.GM.CST.T.L1.1__H.A.MATH.GM.CST.T.L1.10__H.A.MATH.GM.CST.T.L1.11__H.A.MATH.GM.CST.T.L1.12__H.A.MATH.GM.CST.T.L1.13__H.A.MATH.GM.CST.T.L1.2__H.A.MATH.GM.CST.T.L1.3__H.A.MATH.GM.CST.T.L1.4__H.A.MATH.GM.CST.T.L1.5__H.A.MATH.GM.CST.T.L1.6__H.A.MATH.GM.CST.T.L1.7__H.A.MATH.GM.CST.T.L1.8__H.A.MATH.GM.CST.T.L1.9__H.A.MATH.GM.CST.T.L2.1__H.A.MATH.GM.CST.T.L2.2__H.A.MATH.GM.CST.T.L2.3__H.A.MATH.GM.CST.T.L2.4'

@grade = '&grade=8-9-10-11-12'

@video = '&resource_type=video'

@quiz = '&resource_type=assessment'

@hf.each do |skill|

	puts "#{skill[:label]}"
	puts '---------------------------------------------------------'

	@url = "#{skill[:url]}" + '&license=free'

	browser.goto @url
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" }
	puts 'All free resources: ' + browser.div(:class => "filter-section__center", :index => 0).text

	browser.goto @url + @video
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" }
	puts 'All free Videos: ' + browser.div(:class => "filter-section__center", :index => 0).text

	browser.goto @url + @quiz
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" }
	puts 'All free Assessments: ' + browser.div(:class => "filter-section__center", :index => 0).text

	browser.goto @url + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" }
	puts 'All free resources for grades 8-12: ' + browser.div(:class => "filter-section__center", :index => 0).text

	browser.goto @url + @video + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" }
	puts 'All free Videos for grades 8-12: ' + browser.div(:class => "filter-section__center", :index => 0).text

	browser.goto @url + @quiz + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" }
	puts 'All free Assessments for grades 8-12: ' + browser.div(:class => "filter-section__center", :index => 0).text
	puts '---------------------------------------------------------'
	puts ''
	puts ''
end