require 'watir'

browser = Watir::Browser.new(:chrome)

@hf = [
	{
		:label => 'H.A.MATH.OAF.AS Addition and Subtraction',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.AS.ASD.L1.1__H.A.MATH.OAF.AS.ASD.L1.2__H.A.MATH.OAF.AS.ASD.L1.3__H.A.MATH.OAF.AS.ASD.L1.4__H.A.MATH.OAF.AS.ASD.L1.5__H.A.MATH.OAF.AS.ASD.L1.6__H.A.MATH.OAF.AS.ASD.L1.7__H.A.MATH.OAF.AS.ASD.L1.8__H.A.MATH.OAF.AS.ASD.L2.1__H.A.MATH.OAF.AS.ASD.L2.2__H.A.MATH.OAF.AS.ASD.L2.3__H.A.MATH.OAF.AS.ASD.L2.4__H.A.MATH.OAF.AS.ASF.L1.1__H.A.MATH.OAF.AS.ASF.L1.2__H.A.MATH.OAF.AS.ASF.L1.3__H.A.MATH.OAF.AS.ASF.L1.4__H.A.MATH.OAF.AS.ASF.L2.1__H.A.MATH.OAF.AS.ASF.L2.10__H.A.MATH.OAF.AS.ASF.L2.11__H.A.MATH.OAF.AS.ASF.L2.12__H.A.MATH.OAF.AS.ASF.L2.2__H.A.MATH.OAF.AS.ASF.L2.3__H.A.MATH.OAF.AS.ASF.L2.4__H.A.MATH.OAF.AS.ASF.L2.5__H.A.MATH.OAF.AS.ASF.L2.6__H.A.MATH.OAF.AS.ASF.L2.7__H.A.MATH.OAF.AS.ASF.L2.8__H.A.MATH.OAF.AS.ASF.L2.9__H.A.MATH.OAF.AS.ASF.L3.1__H.A.MATH.OAF.AS.ASF.L3.10__H.A.MATH.OAF.AS.ASF.L3.11__H.A.MATH.OAF.AS.ASF.L3.12__H.A.MATH.OAF.AS.ASF.L3.13__H.A.MATH.OAF.AS.ASF.L3.2__H.A.MATH.OAF.AS.ASF.L3.3__H.A.MATH.OAF.AS.ASF.L3.4__H.A.MATH.OAF.AS.ASF.L3.5__H.A.MATH.OAF.AS.ASF.L3.6__H.A.MATH.OAF.AS.ASF.L3.7__H.A.MATH.OAF.AS.ASF.L3.8__H.A.MATH.OAF.AS.ASF.L3.9__H.A.MATH.OAF.AS.ASSN.L1.1__H.A.MATH.OAF.AS.ASSN.L1.2__H.A.MATH.OAF.AS.ASSN.L1.3__H.A.MATH.OAF.AS.ASSN.L1.4__H.A.MATH.OAF.AS.ASSN.L1.5__H.A.MATH.OAF.AS.ASSN.L2__H.A.MATH.OAF.AS.ASSN.L2.1__H.A.MATH.OAF.AS.ASSN.L2.2__H.A.MATH.OAF.AS.ASSN.L2.3__H.A.MATH.OAF.AS.ASSN.L2.4__H.A.MATH.OAF.AS.ASSN.L2.5__H.A.MATH.OAF.AS.AST.L1.1__H.A.MATH.OAF.AS.AST.L1.2__H.A.MATH.OAF.AS.AST.L1.3__H.A.MATH.OAF.AS.AST.L1.4__H.A.MATH.OAF.AS.AST.L2.1__H.A.MATH.OAF.AS.AST.L2.2__H.A.MATH.OAF.AS.AST.L2.3__H.A.MATH.OAF.AS.AST.L2.4__H.A.MATH.OAF.AS.AST.L2.5__H.A.MATH.OAF.AS.AST.L2.6__H.A.MATH.OAF.AS.AST.L2.7__H.A.MATH.OAF.AS.AST.L2.8__H.A.MATH.OAF.AS.ASWN.L1.1__H.A.MATH.OAF.AS.ASWN.L1.10__H.A.MATH.OAF.AS.ASWN.L1.11__H.A.MATH.OAF.AS.ASWN.L1.12__H.A.MATH.OAF.AS.ASWN.L1.13__H.A.MATH.OAF.AS.ASWN.L1.14__H.A.MATH.OAF.AS.ASWN.L1.15__H.A.MATH.OAF.AS.ASWN.L1.16__H.A.MATH.OAF.AS.ASWN.L1.17__H.A.MATH.OAF.AS.ASWN.L1.18__H.A.MATH.OAF.AS.ASWN.L1.19__H.A.MATH.OAF.AS.ASWN.L1.2__H.A.MATH.OAF.AS.ASWN.L1.20__H.A.MATH.OAF.AS.ASWN.L1.21__H.A.MATH.OAF.AS.ASWN.L1.22__H.A.MATH.OAF.AS.ASWN.L1.3__H.A.MATH.OAF.AS.ASWN.L1.4__H.A.MATH.OAF.AS.ASWN.L1.5__H.A.MATH.OAF.AS.ASWN.L1.6__H.A.MATH.OAF.AS.ASWN.L1.7__H.A.MATH.OAF.AS.ASWN.L1.8__H.A.MATH.OAF.AS.ASWN.L1.9__H.A.MATH.OAF.AS.ASWN.L2.1__H.A.MATH.OAF.AS.ASWN.L2.10__H.A.MATH.OAF.AS.ASWN.L2.11__H.A.MATH.OAF.AS.ASWN.L2.12__H.A.MATH.OAF.AS.ASWN.L2.13__H.A.MATH.OAF.AS.ASWN.L2.2__H.A.MATH.OAF.AS.ASWN.L2.3__H.A.MATH.OAF.AS.ASWN.L2.4__H.A.MATH.OAF.AS.ASWN.L2.5__H.A.MATH.OAF.AS.ASWN.L2.6__H.A.MATH.OAF.AS.ASWN.L2.7__H.A.MATH.OAF.AS.ASWN.L2.8__H.A.MATH.OAF.AS.ASWN.L2.9__H.A.MATH.OAF.AS.ASWN.L3.1__H.A.MATH.OAF.AS.ASWN.L3.2__H.A.MATH.OAF.AS.ASWN.L4.1__H.A.MATH.OAF.AS.ASWN.L4.2__H.A.MATH.OAF.AS.FA.L1.1__H.A.MATH.OAF.AS.FA.L1.2__H.A.MATH.OAF.AS.FA.L1.3__H.A.MATH.OAF.AS.FA.L1.4__H.A.MATH.OAF.AS.FA.L1.5__H.A.MATH.OAF.AS.FA.L1.6__H.A.MATH.OAF.AS.FA.L2.1__H.A.MATH.OAF.AS.FA.L2.2__H.A.MATH.OAF.AS.FA.L2.3__H.A.MATH.OAF.AS.FA.L3.1__H.A.MATH.OAF.AS.FA.L3.2__H.A.MATH.OAF.AS.FA.L4.1__H.A.MATH.OAF.AS.FA.L4.2__H.A.MATH.OAF.AS.FA.L4.3__H.A.MATH.OAF.AS.FA.L4.4__H.A.MATH.OAF.AS.FC.L1.1__H.A.MATH.OAF.AS.FC.L1.2__H.A.MATH.OAF.AS.FC.L1.3__H.A.MATH.OAF.AS.FC.L2.1__H.A.MATH.OAF.AS.FC.L2.2__H.A.MATH.OAF.AS.FS.L1.1__H.A.MATH.OAF.AS.FS.L1.2__H.A.MATH.OAF.AS.FS.L1.3__H.A.MATH.OAF.AS.FS.L1.4__H.A.MATH.OAF.AS.FS.L1.5__H.A.MATH.OAF.AS.FS.L1.6__H.A.MATH.OAF.AS.FS.L1.7__H.A.MATH.OAF.AS.FS.L1.8__H.A.MATH.OAF.AS.FS.L2.1__H.A.MATH.OAF.AS.FS.L2.2__H.A.MATH.OAF.AS.FS.L3.1__H.A.MATH.OAF.AS.FS.L3.2__H.A.MATH.OAF.AS.FS.L4.1__H.A.MATH.OAF.AS.MAS.L1.1__H.A.MATH.OAF.AS.MAS.L1.10__H.A.MATH.OAF.AS.MAS.L1.2__H.A.MATH.OAF.AS.MAS.L1.3__H.A.MATH.OAF.AS.MAS.L1.4__H.A.MATH.OAF.AS.MAS.L1.5__H.A.MATH.OAF.AS.MAS.L1.6__H.A.MATH.OAF.AS.MAS.L1.7__H.A.MATH.OAF.AS.MAS.L1.8__H.A.MATH.OAF.AS.MAS.L1.9__H.A.MATH.OAF.AS.MAS.L2.1__H.A.MATH.OAF.AS.MAS.L2.2__H.A.MATH.OAF.AS.MAS.L2.3__H.A.MATH.OAF.AS.MAS.L2.4__H.A.MATH.OAF.AS.MAS.L2.5__H.A.MATH.OAF.AS.RAS.L1.1__H.A.MATH.OAF.AS.RAS.L1.2__H.A.MATH.OAF.AS.RAS.L1.3__H.A.MATH.OAF.AS.RAS.L1.4__H.A.MATH.OAF.AS.RAS.L1.5__H.A.MATH.OAF.AS.RAS.L1.6__H.A.MATH.OAF.AS.RAS.L2.1__H.A.MATH.OAF.AS.RAS.L2.2__H.A.MATH.OAF.AS.RAS.L2.3__H.A.MATH.OAF.AS.RAS.L3.1__H.A.MATH.OAF.AS.RAS.L3.10__H.A.MATH.OAF.AS.RAS.L3.2__H.A.MATH.OAF.AS.RAS.L3.3__H.A.MATH.OAF.AS.RAS.L3.4__H.A.MATH.OAF.AS.RAS.L3.5__H.A.MATH.OAF.AS.RAS.L3.6__H.A.MATH.OAF.AS.RAS.L3.7__H.A.MATH.OAF.AS.RAS.L3.8__H.A.MATH.OAF.AS.RAS.L3.9'
	},
	{
		:label => 'H.A.MATH.OAF.E Estimation',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.E.CE.L1.1__H.A.MATH.OAF.E.CE.L1.2__H.A.MATH.OAF.E.CE.L1.3__H.A.MATH.OAF.E.CE.L2.1__H.A.MATH.OAF.E.CE.L2.10__H.A.MATH.OAF.E.CE.L2.2__H.A.MATH.OAF.E.CE.L2.3__H.A.MATH.OAF.E.CE.L2.4__H.A.MATH.OAF.E.CE.L2.5__H.A.MATH.OAF.E.CE.L2.6__H.A.MATH.OAF.E.CE.L2.7__H.A.MATH.OAF.E.CE.L2.8__H.A.MATH.OAF.E.CE.L2.9__H.A.MATH.OAF.E.CE.L3.1__H.A.MATH.OAF.E.CE.L3.2__H.A.MATH.OAF.E.CE.L3.3__H.A.MATH.OAF.E.CE.L3.4__H.A.MATH.OAF.E.CE.L3.5__H.A.MATH.OAF.E.CE.L3.6'
	},
	{
		:label => 'H.A.MATH.OAF.EI Equations and Inequalities',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.EI.CLEI.L1.1__H.A.MATH.OAF.EI.CLEI.L1.2__H.A.MATH.OAF.EI.CLEI.L1.3__H.A.MATH.OAF.EI.CLEI.L2.1__H.A.MATH.OAF.EI.CLEI.L2.2__H.A.MATH.OAF.EI.CLEI.L2.3__H.A.MATH.OAF.EI.SLEI.L1.10__H.A.MATH.OAF.EI.SLEI.L1.11__H.A.MATH.OAF.EI.SLEI.L1.12__H.A.MATH.OAF.EI.SLEI.L1.13__H.A.MATH.OAF.EI.SLEI.L1.14__H.A.MATH.OAF.EI.SLEI.L1.15__H.A.MATH.OAF.EI.SLEI.L1.16__H.A.MATH.OAF.EI.SLEI.L1.17__H.A.MATH.OAF.EI.SLEI.L1.18__H.A.MATH.OAF.EI.SLEI.L1.19__H.A.MATH.OAF.EI.SLEI.L1.2__H.A.MATH.OAF.EI.SLEI.L1.3__H.A.MATH.OAF.EI.SLEI.L1.4__H.A.MATH.OAF.EI.SLEI.L1.5__H.A.MATH.OAF.EI.SLEI.L1.6__H.A.MATH.OAF.EI.SLEI.L1.7__H.A.MATH.OAF.EI.SLEI.L1.8__H.A.MATH.OAF.EI.SLEI.L1.9__H.A.MATH.OAF.EI.SLEI.L2.1__H.A.MATH.OAF.EI.SLEI.L2.10__H.A.MATH.OAF.EI.SLEI.L2.2__H.A.MATH.OAF.EI.SLEI.L2.3__H.A.MATH.OAF.EI.SLEI.L2.4__H.A.MATH.OAF.EI.SLEI.L2.5__H.A.MATH.OAF.EI.SLEI.L2.6__H.A.MATH.OAF.EI.SLEI.L2.7__H.A.MATH.OAF.EI.SLEI.L2.8__H.A.MATH.OAF.EI.SLEI.L2.9__H.A.MATH.OAF.EI.SLEI.L3.1__H.A.MATH.OAF.EI.SLEI.L3.10__H.A.MATH.OAF.EI.SLEI.L3.11__H.A.MATH.OAF.EI.SLEI.L3.2__H.A.MATH.OAF.EI.SLEI.L3.3__H.A.MATH.OAF.EI.SLEI.L3.4__H.A.MATH.OAF.EI.SLEI.L3.5__H.A.MATH.OAF.EI.SLEI.L3.6__H.A.MATH.OAF.EI.SLEI.L3.7__H.A.MATH.OAF.EI.SLEI.L3.8__H.A.MATH.OAF.EI.SLEI.L3.9__H.A.MATH.OAF.EI.SYS.L1.1__H.A.MATH.OAF.EI.SYS.L1.10__H.A.MATH.OAF.EI.SYS.L1.11__H.A.MATH.OAF.EI.SYS.L1.2__H.A.MATH.OAF.EI.SYS.L1.3__H.A.MATH.OAF.EI.SYS.L1.4__H.A.MATH.OAF.EI.SYS.L1.5__H.A.MATH.OAF.EI.SYS.L1.6__H.A.MATH.OAF.EI.SYS.L1.7__H.A.MATH.OAF.EI.SYS.L1.8__H.A.MATH.OAF.EI.SYS.L1.9__H.A.MATH.OAF.EI.SYS.L2.1__H.A.MATH.OAF.EI.SYS.L3.1'
	},
	{
		:label => 'H.A.MATH.OAF.ELEF Exponential and Logarithmic Equations and Function',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.ELEF.EE.L1.1__H.A.MATH.OAF.ELEF.EE.L1.2__H.A.MATH.OAF.ELEF.EE.L1.3__H.A.MATH.OAF.ELEF.EE.L1.4__H.A.MATH.OAF.ELEF.EE.L1.5__H.A.MATH.OAF.ELEF.EE.L1.6__H.A.MATH.OAF.ELEF.EE.L1.7__H.A.MATH.OAF.ELEF.EE.L2.1__H.A.MATH.OAF.ELEF.EE.L2.2__H.A.MATH.OAF.ELEF.EE.L2.3__H.A.MATH.OAF.ELEF.EE.L2.4__H.A.MATH.OAF.ELEF.EE.L2.5__H.A.MATH.OAF.ELEF.EE.L2.6__H.A.MATH.OAF.ELEF.EG.L1.1__H.A.MATH.OAF.ELEF.EG.L1.10__H.A.MATH.OAF.ELEF.EG.L1.11__H.A.MATH.OAF.ELEF.EG.L1.2__H.A.MATH.OAF.ELEF.EG.L1.3__H.A.MATH.OAF.ELEF.EG.L1.4__H.A.MATH.OAF.ELEF.EG.L1.5__H.A.MATH.OAF.ELEF.EG.L1.6__H.A.MATH.OAF.ELEF.EG.L1.7__H.A.MATH.OAF.ELEF.EG.L1.8__H.A.MATH.OAF.ELEF.EG.L1.9__H.A.MATH.OAF.ELEF.EG.L2.1__H.A.MATH.OAF.ELEF.EG.L2.2__H.A.MATH.OAF.ELEF.EG.L2.3__H.A.MATH.OAF.ELEF.EG.L2.4'
	},
	{
		:label => 'H.A.MATH.OAF.ES Equal Sign',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.ES.ESIE.L1.1__H.A.MATH.OAF.ES.ESIE.L1.2__H.A.MATH.OAF.ES.ESIE.L1.3__H.A.MATH.OAF.ES.ESIE.L1.4__H.A.MATH.OAF.ES.ESIE.L2.1__H.A.MATH.OAF.ES.ESIE.L2.2__H.A.MATH.OAF.ES.ESIE.L2.3__H.A.MATH.OAF.ES.ESIE.L2.4__H.A.MATH.OAF.ES.ESIE.L2.5__H.A.MATH.OAF.ES.ESIE.L3.1__H.A.MATH.OAF.ES.ESIE.L3.2__H.A.MATH.OAF.ES.ESIE.L3.3__H.A.MATH.OAF.ES.ESIE.L3.4__H.A.MATH.OAF.ES.ESIE.L3.5__H.A.MATH.OAF.ES.ESIE.L3.6__H.A.MATH.OAF.ES.ESIE.L4.1__H.A.MATH.OAF.ES.ESIE.L4.2__H.A.MATH.OAF.ES.ESIE.L4.3__H.A.MATH.OAF.ES.ESIE.L4.4__H.A.MATH.OAF.ES.ESIE.L4.5__H.A.MATH.OAF.ES.ESIE.L4.6__H.A.MATH.OAF.ES.ESIE.L4.7'
	},
	{
		:label => 'H.A.MATH.OAF.ET Elapsed Time',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.ET.ET.L1.1__H.A.MATH.OAF.ET.ET.L1.2__H.A.MATH.OAF.ET.ET.L1.3__H.A.MATH.OAF.ET.ET.L2.1__H.A.MATH.OAF.ET.ET.L2.2__H.A.MATH.OAF.ET.ET.L2.3__H.A.MATH.OAF.ET.ET.L2.4__H.A.MATH.OAF.ET.ET.L3.1__H.A.MATH.OAF.ET.ET.L3.2__H.A.MATH.OAF.ET.ET.L3.3__H.A.MATH.OAF.ET.ET.L3.4__H.A.MATH.OAF.ET.ET.L3.5__H.A.MATH.OAF.ET.ET.L4.1__H.A.MATH.OAF.ET.ET.L4.2__H.A.MATH.OAF.ET.ET.L5.1__H.A.MATH.OAF.ET.ET.L5.2__H.A.MATH.OAF.ET.ET.L5.3'
	},
	{
		:label => 'H.A.MATH.OAF.EX Expressions',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.EX.AE.L1.1__H.A.MATH.OAF.EX.AE.L1.10__H.A.MATH.OAF.EX.AE.L1.11__H.A.MATH.OAF.EX.AE.L1.12__H.A.MATH.OAF.EX.AE.L1.13__H.A.MATH.OAF.EX.AE.L1.14__H.A.MATH.OAF.EX.AE.L1.2__H.A.MATH.OAF.EX.AE.L1.3__H.A.MATH.OAF.EX.AE.L1.4__H.A.MATH.OAF.EX.AE.L1.5__H.A.MATH.OAF.EX.AE.L1.6__H.A.MATH.OAF.EX.AE.L1.7__H.A.MATH.OAF.EX.AE.L1.8__H.A.MATH.OAF.EX.AE.L1.9__H.A.MATH.OAF.EX.AE.L2.1__H.A.MATH.OAF.EX.AE.L3.1__H.A.MATH.OAF.EX.AE.L3.2__H.A.MATH.OAF.EX.AE.L3.3__H.A.MATH.OAF.EX.AE.L3.4__H.A.MATH.OAF.EX.AE.L3.5__H.A.MATH.OAF.EX.AE.L3.6__H.A.MATH.OAF.EX.V.L1.1__H.A.MATH.OAF.EX.V.L1.2__H.A.MATH.OAF.EX.V.L1.3__H.A.MATH.OAF.EX.V.L1.4__H.A.MATH.OAF.EX.V.L2.1__H.A.MATH.OAF.EX.V.L3.1__H.A.MATH.OAF.EX.V.L3.2__H.A.MATH.OAF.EX.V.L3.3'
	},
	{
		:label => 'H.A.MATH.OAF.FC Function Concepts',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.FC.CM.L1.1__H.A.MATH.OAF.FC.CM.L1.2__H.A.MATH.OAF.FC.CM.L1.3__H.A.MATH.OAF.FC.CM.L1.4__H.A.MATH.OAF.FC.CM.L1.5__H.A.MATH.OAF.FC.CM.L1.6__H.A.MATH.OAF.FC.CM.L2.1__H.A.MATH.OAF.FC.CM.L2.2__H.A.MATH.OAF.FC.CM.L2.3__H.A.MATH.OAF.FC.CM.L2.4__H.A.MATH.OAF.FC.CM.L3.1__H.A.MATH.OAF.FC.CM.L3.2__H.A.MATH.OAF.FC.CM.L3.3__H.A.MATH.OAF.FC.CM.L3.4__H.A.MATH.OAF.FC.CM.L3.5__H.A.MATH.OAF.FC.CM.L3.6__H.A.MATH.OAF.FC.CM.L3.7__H.A.MATH.OAF.FC.CM.L3.8__H.A.MATH.OAF.FC.CM.L3.9__H.A.MATH.OAF.FC.FB.L1.1__H.A.MATH.OAF.FC.FB.L1.10__H.A.MATH.OAF.FC.FB.L1.11__H.A.MATH.OAF.FC.FB.L1.12__H.A.MATH.OAF.FC.FB.L1.13__H.A.MATH.OAF.FC.FB.L1.14__H.A.MATH.OAF.FC.FB.L1.2__H.A.MATH.OAF.FC.FB.L1.3__H.A.MATH.OAF.FC.FB.L1.4__H.A.MATH.OAF.FC.FB.L1.5__H.A.MATH.OAF.FC.FB.L1.6__H.A.MATH.OAF.FC.FB.L1.7__H.A.MATH.OAF.FC.FB.L1.8__H.A.MATH.OAF.FC.FB.L1.9__H.A.MATH.OAF.FC.FB.L2.1__H.A.MATH.OAF.FC.FB.L2.2__H.A.MATH.OAF.FC.FB.L2.3__H.A.MATH.OAF.FC.FB.L2.4__H.A.MATH.OAF.FC.FB.L2.5__H.A.MATH.OAF.FC.FB.L2.6__H.A.MATH.OAF.FC.FB.L2.7__H.A.MATH.OAF.FC.FB.L2.8__H.A.MATH.OAF.FC.FB.L2.9__H.A.MATH.OAF.FC.FB.L3.1__H.A.MATH.OAF.FC.FB.L3.10__H.A.MATH.OAF.FC.FB.L3.11__H.A.MATH.OAF.FC.FB.L3.12__H.A.MATH.OAF.FC.FB.L3.13__H.A.MATH.OAF.FC.FB.L3.14__H.A.MATH.OAF.FC.FB.L3.15__H.A.MATH.OAF.FC.FB.L3.16__H.A.MATH.OAF.FC.FB.L3.17__H.A.MATH.OAF.FC.FB.L3.18__H.A.MATH.OAF.FC.FB.L3.19__H.A.MATH.OAF.FC.FB.L3.2__H.A.MATH.OAF.FC.FB.L3.20__H.A.MATH.OAF.FC.FB.L3.21__H.A.MATH.OAF.FC.FB.L3.22__H.A.MATH.OAF.FC.FB.L3.23__H.A.MATH.OAF.FC.FB.L3.24__H.A.MATH.OAF.FC.FB.L3.25__H.A.MATH.OAF.FC.FB.L3.26__H.A.MATH.OAF.FC.FB.L3.27__H.A.MATH.OAF.FC.FB.L3.28__H.A.MATH.OAF.FC.FB.L3.29__H.A.MATH.OAF.FC.FB.L3.3__H.A.MATH.OAF.FC.FB.L3.30__H.A.MATH.OAF.FC.FB.L3.31__H.A.MATH.OAF.FC.FB.L3.32__H.A.MATH.OAF.FC.FB.L3.33__H.A.MATH.OAF.FC.FB.L3.34__H.A.MATH.OAF.FC.FB.L3.35__H.A.MATH.OAF.FC.FB.L3.36__H.A.MATH.OAF.FC.FB.L3.37__H.A.MATH.OAF.FC.FB.L3.38__H.A.MATH.OAF.FC.FB.L3.39__H.A.MATH.OAF.FC.FB.L3.4__H.A.MATH.OAF.FC.FB.L3.5__H.A.MATH.OAF.FC.FB.L3.6__H.A.MATH.OAF.FC.FB.L3.7__H.A.MATH.OAF.FC.FB.L3.8__H.A.MATH.OAF.FC.FB.L3.9__H.A.MATH.OAF.FC.FB.L4.1__H.A.MATH.OAF.FC.FB.L4.2__H.A.MATH.OAF.FC.FB.L4.3__H.A.MATH.OAF.FC.FB.L4.4__H.A.MATH.OAF.FC.SIPL.L1.1__H.A.MATH.OAF.FC.SIPL.L1.10__H.A.MATH.OAF.FC.SIPL.L1.11__H.A.MATH.OAF.FC.SIPL.L1.12__H.A.MATH.OAF.FC.SIPL.L1.13__H.A.MATH.OAF.FC.SIPL.L1.14__H.A.MATH.OAF.FC.SIPL.L1.15__H.A.MATH.OAF.FC.SIPL.L1.16__H.A.MATH.OAF.FC.SIPL.L1.17__H.A.MATH.OAF.FC.SIPL.L1.18__H.A.MATH.OAF.FC.SIPL.L1.19__H.A.MATH.OAF.FC.SIPL.L1.2__H.A.MATH.OAF.FC.SIPL.L1.20__H.A.MATH.OAF.FC.SIPL.L1.21__H.A.MATH.OAF.FC.SIPL.L1.22__H.A.MATH.OAF.FC.SIPL.L1.23__H.A.MATH.OAF.FC.SIPL.L1.24__H.A.MATH.OAF.FC.SIPL.L1.3__H.A.MATH.OAF.FC.SIPL.L1.4__H.A.MATH.OAF.FC.SIPL.L1.5__H.A.MATH.OAF.FC.SIPL.L1.6__H.A.MATH.OAF.FC.SIPL.L1.7__H.A.MATH.OAF.FC.SIPL.L1.8__H.A.MATH.OAF.FC.SIPL.L1.9__H.A.MATH.OAF.FC.SIPL.L2.1__H.A.MATH.OAF.FC.SIPL.L2.10__H.A.MATH.OAF.FC.SIPL.L2.11__H.A.MATH.OAF.FC.SIPL.L2.12__H.A.MATH.OAF.FC.SIPL.L2.13__H.A.MATH.OAF.FC.SIPL.L2.14__H.A.MATH.OAF.FC.SIPL.L2.15__H.A.MATH.OAF.FC.SIPL.L2.2__H.A.MATH.OAF.FC.SIPL.L2.3__H.A.MATH.OAF.FC.SIPL.L2.4__H.A.MATH.OAF.FC.SIPL.L2.5__H.A.MATH.OAF.FC.SIPL.L2.6__H.A.MATH.OAF.FC.SIPL.L2.7__H.A.MATH.OAF.FC.SIPL.L2.8__H.A.MATH.OAF.FC.SIPL.L2.9'
	},
	{
		:label => 'H.A.MATH.OAF.MD Multiplication and Division',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.MD.FMD.L1.1__H.A.MATH.OAF.MD.FMD.L1.10__H.A.MATH.OAF.MD.FMD.L1.11__H.A.MATH.OAF.MD.FMD.L1.2__H.A.MATH.OAF.MD.FMD.L1.3__H.A.MATH.OAF.MD.FMD.L1.4__H.A.MATH.OAF.MD.FMD.L1.5__H.A.MATH.OAF.MD.FMD.L1.6__H.A.MATH.OAF.MD.FMD.L1.7__H.A.MATH.OAF.MD.FMD.L1.8__H.A.MATH.OAF.MD.FMD.L1.9__H.A.MATH.OAF.MD.FMD.L2.1__H.A.MATH.OAF.MD.FMD.L2.10__H.A.MATH.OAF.MD.FMD.L2.11__H.A.MATH.OAF.MD.FMD.L2.12__H.A.MATH.OAF.MD.FMD.L2.13__H.A.MATH.OAF.MD.FMD.L2.14__H.A.MATH.OAF.MD.FMD.L2.15__H.A.MATH.OAF.MD.FMD.L2.16__H.A.MATH.OAF.MD.FMD.L2.17__H.A.MATH.OAF.MD.FMD.L2.18__H.A.MATH.OAF.MD.FMD.L2.19__H.A.MATH.OAF.MD.FMD.L2.2__H.A.MATH.OAF.MD.FMD.L2.20__H.A.MATH.OAF.MD.FMD.L2.21__H.A.MATH.OAF.MD.FMD.L2.22__H.A.MATH.OAF.MD.FMD.L2.23__H.A.MATH.OAF.MD.FMD.L2.24__H.A.MATH.OAF.MD.FMD.L2.25__H.A.MATH.OAF.MD.FMD.L2.26__H.A.MATH.OAF.MD.FMD.L2.27__H.A.MATH.OAF.MD.FMD.L2.3__H.A.MATH.OAF.MD.FMD.L2.4__H.A.MATH.OAF.MD.FMD.L2.5__H.A.MATH.OAF.MD.FMD.L2.6__H.A.MATH.OAF.MD.FMD.L2.7__H.A.MATH.OAF.MD.FMD.L2.8__H.A.MATH.OAF.MD.FMD.L2.9__H.A.MATH.OAF.MD.FMD.L3.1__H.A.MATH.OAF.MD.FMD.L3.2__H.A.MATH.OAF.MD.FMD.L3.3__H.A.MATH.OAF.MD.FMD.L3.4__H.A.MATH.OAF.MD.FMD.L3.5__H.A.MATH.OAF.MD.FMD.L3.6__H.A.MATH.OAF.MD.MDD.L1.1__H.A.MATH.OAF.MD.MDD.L1.2__H.A.MATH.OAF.MD.MDD.L1.3__H.A.MATH.OAF.MD.MDD.L2.1__H.A.MATH.OAF.MD.MDD.L3.1__H.A.MATH.OAF.MD.MDD.L4.1__H.A.MATH.OAF.MD.MDD.L4.2__H.A.MATH.OAF.MD.MDD.L5.1__H.A.MATH.OAF.MD.MDD.L5.2__H.A.MATH.OAF.MD.MDD.L5.3__H.A.MATH.OAF.MD.MDD.L5.4__H.A.MATH.OAF.MD.MDD.L5.5__H.A.MATH.OAF.MD.MDF.L1.1__H.A.MATH.OAF.MD.MDF.L1.2__H.A.MATH.OAF.MD.MDF.L1.3__H.A.MATH.OAF.MD.MDF.L1.4__H.A.MATH.OAF.MD.MDF.L1.5__H.A.MATH.OAF.MD.MDF.L1.6__H.A.MATH.OAF.MD.MDF.L1.7__H.A.MATH.OAF.MD.MDF.L2.1__H.A.MATH.OAF.MD.MDF.L2.10__H.A.MATH.OAF.MD.MDF.L2.2__H.A.MATH.OAF.MD.MDF.L2.3__H.A.MATH.OAF.MD.MDF.L2.4__H.A.MATH.OAF.MD.MDF.L2.5__H.A.MATH.OAF.MD.MDF.L2.6__H.A.MATH.OAF.MD.MDF.L2.7__H.A.MATH.OAF.MD.MDF.L2.8__H.A.MATH.OAF.MD.MDF.L2.9__H.A.MATH.OAF.MD.MDF.L3.1__H.A.MATH.OAF.MD.MDF.L3.2__H.A.MATH.OAF.MD.MDF.L3.3__H.A.MATH.OAF.MD.MDF.L3.4__H.A.MATH.OAF.MD.MDF.L3.5__H.A.MATH.OAF.MD.MDF.L4.1__H.A.MATH.OAF.MD.MDF.L4.2__H.A.MATH.OAF.MD.MDF.L4.3__H.A.MATH.OAF.MD.MDF.L4.4__H.A.MATH.OAF.MD.MDF.L4.5__H.A.MATH.OAF.MD.MDF.L4.6__H.A.MATH.OAF.MD.MDF.L4.7__H.A.MATH.OAF.MD.MDSN.L1.1__H.A.MATH.OAF.MD.MDSN.L1.2__H.A.MATH.OAF.MD.MDSN.L1.3__H.A.MATH.OAF.MD.MDSN.L1.4__H.A.MATH.OAF.MD.MDSN.L1.5__H.A.MATH.OAF.MD.MDSN.L2.1__H.A.MATH.OAF.MD.MDSN.L2.2__H.A.MATH.OAF.MD.MDSN.L2.3__H.A.MATH.OAF.MD.MDSN.L2.4__H.A.MATH.OAF.MD.MDWN.L1.1__H.A.MATH.OAF.MD.MDWN.L1.10__H.A.MATH.OAF.MD.MDWN.L1.11__H.A.MATH.OAF.MD.MDWN.L1.12__H.A.MATH.OAF.MD.MDWN.L1.13__H.A.MATH.OAF.MD.MDWN.L1.14__H.A.MATH.OAF.MD.MDWN.L1.15__H.A.MATH.OAF.MD.MDWN.L1.16__H.A.MATH.OAF.MD.MDWN.L1.17__H.A.MATH.OAF.MD.MDWN.L1.18__H.A.MATH.OAF.MD.MDWN.L1.19__H.A.MATH.OAF.MD.MDWN.L1.2__H.A.MATH.OAF.MD.MDWN.L1.20__H.A.MATH.OAF.MD.MDWN.L1.3__H.A.MATH.OAF.MD.MDWN.L1.4__H.A.MATH.OAF.MD.MDWN.L1.5__H.A.MATH.OAF.MD.MDWN.L1.6__H.A.MATH.OAF.MD.MDWN.L1.7__H.A.MATH.OAF.MD.MDWN.L1.8__H.A.MATH.OAF.MD.MDWN.L1.9__H.A.MATH.OAF.MD.MDWN.L2.1__H.A.MATH.OAF.MD.MDWN.L2.10__H.A.MATH.OAF.MD.MDWN.L2.11__H.A.MATH.OAF.MD.MDWN.L2.2__H.A.MATH.OAF.MD.MDWN.L2.3__H.A.MATH.OAF.MD.MDWN.L2.4__H.A.MATH.OAF.MD.MDWN.L2.5__H.A.MATH.OAF.MD.MDWN.L2.6__H.A.MATH.OAF.MD.MDWN.L2.7__H.A.MATH.OAF.MD.MDWN.L2.8__H.A.MATH.OAF.MD.MDWN.L2.9__H.A.MATH.OAF.MD.MDWN.L3.1__H.A.MATH.OAF.MD.MDWN.L3.10__H.A.MATH.OAF.MD.MDWN.L3.11__H.A.MATH.OAF.MD.MDWN.L3.12__H.A.MATH.OAF.MD.MDWN.L3.13__H.A.MATH.OAF.MD.MDWN.L3.14__H.A.MATH.OAF.MD.MDWN.L3.15__H.A.MATH.OAF.MD.MDWN.L3.2__H.A.MATH.OAF.MD.MDWN.L3.3__H.A.MATH.OAF.MD.MDWN.L3.4__H.A.MATH.OAF.MD.MDWN.L3.5__H.A.MATH.OAF.MD.MDWN.L3.6__H.A.MATH.OAF.MD.MDWN.L3.7__H.A.MATH.OAF.MD.MDWN.L3.8__H.A.MATH.OAF.MD.MDWN.L3.9__H.A.MATH.OAF.MD.MDWN.L4.1__H.A.MATH.OAF.MD.MDWN.L4.2__H.A.MATH.OAF.MD.MDWN.L4.3__H.A.MATH.OAF.MD.MDWN.L4.4__H.A.MATH.OAF.MD.MDWN.L4.5__H.A.MATH.OAF.MD.MDWN.L5.1__H.A.MATH.OAF.MD.MDWN.L5.2__H.A.MATH.OAF.MD.MDWN.L5.3__H.A.MATH.OAF.MD.MDWN.L5.4__H.A.MATH.OAF.MD.MDWN.L6.1__H.A.MATH.OAF.MD.MDWN.L6.2__H.A.MATH.OAF.MD.MDWN.L6.3__H.A.MATH.OAF.MD.MDWN.L6.4__H.A.MATH.OAF.MD.MDWN.L6.5__H.A.MATH.OAF.MD.MDWN.L6.6__H.A.MATH.OAF.MD.MDWN.L7.1__H.A.MATH.OAF.MD.MDWN.L7.2__H.A.MATH.OAF.MD.MDWN.L7.3__H.A.MATH.OAF.MD.MDWN.L7.4__H.A.MATH.OAF.MD.MDWN.L7.5__H.A.MATH.OAF.MD.MDWN.L7.6'
	},
	{
		:label => 'H.A.MATH.OAF.MP Multistep Problems',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.MP.MPD.L1.1__H.A.MATH.OAF.MP.MPD.L2.1__H.A.MATH.OAF.MP.MPD.L2.2__H.A.MATH.OAF.MP.MPF.L1.1__H.A.MATH.OAF.MP.MPF.L1.2__H.A.MATH.OAF.MP.MPF.L1.3__H.A.MATH.OAF.MP.MPF.L2.1__H.A.MATH.OAF.MP.MPF.L2.2__H.A.MATH.OAF.MP.MPSN.L1.1__H.A.MATH.OAF.MP.MPSN.L1.2__H.A.MATH.OAF.MP.MPSN.L2.1__H.A.MATH.OAF.MP.MPSN.L2.2__H.A.MATH.OAF.MP.MPTM.L2.1__H.A.MATH.OAF.MP.MPWN.L1.1__H.A.MATH.OAF.MP.MPWN.L1.2__H.A.MATH.OAF.MP.MPWN.L1.3__H.A.MATH.OAF.MP.MPWN.L1.4__H.A.MATH.OAF.MP.MPWN.L1.5__H.A.MATH.OAF.MP.MPWN.L2.1__H.A.MATH.OAF.MP.MPWN.L2.2__H.A.MATH.OAF.MP.MPWN.L2.3__H.A.MATH.OAF.MP.MPWN.L2.4'
	},
	{
		:label => 'H.A.MATH.OAF.OM Operations with Money',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.OM.OM.L1.1__H.A.MATH.OAF.OM.OM.L1.2__H.A.MATH.OAF.OM.OM.L1.3__H.A.MATH.OAF.OM.OM.L1.4__H.A.MATH.OAF.OM.OM.L2.1__H.A.MATH.OAF.OM.OM.L2.2__H.A.MATH.OAF.OM.OM.L3.1__H.A.MATH.OAF.OM.OM.L3.2__H.A.MATH.OAF.OM.OM.L3.3__H.A.MATH.OAF.OM.OM.L3.4__H.A.MATH.OAF.OM.OM.L3.5__H.A.MATH.OAF.OM.OM.L3.6__H.A.MATH.OAF.OM.OM.L3.7__H.A.MATH.OAF.OM.OM.L3.8'
	},
	{
		:label => 'H.A.MATH.OAF.ORCN Operations with Real and Complex Numbers',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.ORCN.CN.L1.1__H.A.MATH.OAF.ORCN.CN.L1.2__H.A.MATH.OAF.ORCN.CN.L1.3__H.A.MATH.OAF.ORCN.CN.L1.4__H.A.MATH.OAF.ORCN.CN.L1.5__H.A.MATH.OAF.ORCN.ER.L1.1__H.A.MATH.OAF.ORCN.ER.L1.10__H.A.MATH.OAF.ORCN.ER.L1.11__H.A.MATH.OAF.ORCN.ER.L1.12__H.A.MATH.OAF.ORCN.ER.L1.13__H.A.MATH.OAF.ORCN.ER.L1.14__H.A.MATH.OAF.ORCN.ER.L1.15__H.A.MATH.OAF.ORCN.ER.L1.16__H.A.MATH.OAF.ORCN.ER.L1.2__H.A.MATH.OAF.ORCN.ER.L1.3__H.A.MATH.OAF.ORCN.ER.L1.4__H.A.MATH.OAF.ORCN.ER.L1.5__H.A.MATH.OAF.ORCN.ER.L1.6__H.A.MATH.OAF.ORCN.ER.L1.7__H.A.MATH.OAF.ORCN.ER.L1.8__H.A.MATH.OAF.ORCN.ER.L1.9__H.A.MATH.OAF.ORCN.ER.L2.1__H.A.MATH.OAF.ORCN.ER.L2.2__H.A.MATH.OAF.ORCN.ER.L2.3__H.A.MATH.OAF.ORCN.ER.L2.4__H.A.MATH.OAF.ORCN.ER.L2.5__H.A.MATH.OAF.ORCN.ER.L2.6'
	},
	{
		:label => 'H.A.MATH.OAF.QPEF Quadratic and Polynomial Equations and Functions',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.QPEF.PE.L1.1__H.A.MATH.OAF.QPEF.PE.L1.2__H.A.MATH.OAF.QPEF.PE.L1.3__H.A.MATH.OAF.QPEF.PE.L1.4__H.A.MATH.OAF.QPEF.PE.L1.5__H.A.MATH.OAF.QPEF.PE.L1.6__H.A.MATH.OAF.QPEF.PE.L1.7__H.A.MATH.OAF.QPEF.PE.L1.8__H.A.MATH.OAF.QPEF.PE.L1.9__H.A.MATH.OAF.QPEF.PE.L2.1__H.A.MATH.OAF.QPEF.PE.L2.10__H.A.MATH.OAF.QPEF.PE.L2.11__H.A.MATH.OAF.QPEF.PE.L2.12__H.A.MATH.OAF.QPEF.PE.L2.2__H.A.MATH.OAF.QPEF.PE.L2.3__H.A.MATH.OAF.QPEF.PE.L2.4__H.A.MATH.OAF.QPEF.PE.L2.5__H.A.MATH.OAF.QPEF.PE.L2.6__H.A.MATH.OAF.QPEF.PE.L2.7__H.A.MATH.OAF.QPEF.PE.L2.8__H.A.MATH.OAF.QPEF.PE.L2.9__H.A.MATH.OAF.QPEF.PE.L3.1__H.A.MATH.OAF.QPEF.PE.L3.2__H.A.MATH.OAF.QPEF.PE.L3.3__H.A.MATH.OAF.QPEF.PE.L3.4__H.A.MATH.OAF.QPEF.PE.L3.5__H.A.MATH.OAF.QPEF.PE.L3.6__H.A.MATH.OAF.QPEF.QEI.L1.1__H.A.MATH.OAF.QPEF.QEI.L1.2__H.A.MATH.OAF.QPEF.QEI.L1.3__H.A.MATH.OAF.QPEF.QEI.L1.4__H.A.MATH.OAF.QPEF.QEI.L2.1__H.A.MATH.OAF.QPEF.QEI.L2.2__H.A.MATH.OAF.QPEF.QEI.L2.3__H.A.MATH.OAF.QPEF.QEI.L2.4__H.A.MATH.OAF.QPEF.QEI.L3.1__H.A.MATH.OAF.QPEF.QEI.L3.2__H.A.MATH.OAF.QPEF.QEI.L3.3__H.A.MATH.OAF.QPEF.QEI.L3.4__H.A.MATH.OAF.QPEF.QEI.L3.5__H.A.MATH.OAF.QPEF.QG.L1.1__H.A.MATH.OAF.QPEF.QG.L1.2__H.A.MATH.OAF.QPEF.QG.L1.3__H.A.MATH.OAF.QPEF.QG.L1.4__H.A.MATH.OAF.QPEF.QG.L1.5__H.A.MATH.OAF.QPEF.QG.L2.1__H.A.MATH.OAF.QPEF.QG.L2.2__H.A.MATH.OAF.QPEF.QG.L2.3__H.A.MATH.OAF.QPEF.QG.L2.4__H.A.MATH.OAF.QPEF.QG.L2.5__H.A.MATH.OAF.QPEF.QG.L2.6'
	},
	{
		:label => 'H.A.MATH.OAF.RP Ratio and Proportion',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.RP.P.L1.1__H.A.MATH.OAF.RP.P.L1.2__H.A.MATH.OAF.RP.P.L1.3__H.A.MATH.OAF.RP.P.L1.4__H.A.MATH.OAF.RP.P.L1.5__H.A.MATH.OAF.RP.P.L1.6__H.A.MATH.OAF.RP.P.L1.7__H.A.MATH.OAF.RP.P.L2.1__H.A.MATH.OAF.RP.P.L2.2__H.A.MATH.OAF.RP.P.L2.3__H.A.MATH.OAF.RP.P.L2.4__H.A.MATH.OAF.RP.P.L2.5__H.A.MATH.OAF.RP.P.L2.6__H.A.MATH.OAF.RP.P.L2.7__H.A.MATH.OAF.RP.P.L3.1__H.A.MATH.OAF.RP.P.L3.2__H.A.MATH.OAF.RP.P.L3.3__H.A.MATH.OAF.RP.PER.L1.1__H.A.MATH.OAF.RP.PER.L1.2__H.A.MATH.OAF.RP.PER.L1.3__H.A.MATH.OAF.RP.PER.L1.4__H.A.MATH.OAF.RP.R.L1.1__H.A.MATH.OAF.RP.R.L1.10__H.A.MATH.OAF.RP.R.L1.11__H.A.MATH.OAF.RP.R.L1.12__H.A.MATH.OAF.RP.R.L1.13__H.A.MATH.OAF.RP.R.L1.14__H.A.MATH.OAF.RP.R.L1.15__H.A.MATH.OAF.RP.R.L1.16__H.A.MATH.OAF.RP.R.L1.17__H.A.MATH.OAF.RP.R.L1.18__H.A.MATH.OAF.RP.R.L1.19__H.A.MATH.OAF.RP.R.L1.2__H.A.MATH.OAF.RP.R.L1.20__H.A.MATH.OAF.RP.R.L1.21__H.A.MATH.OAF.RP.R.L1.22__H.A.MATH.OAF.RP.R.L1.23__H.A.MATH.OAF.RP.R.L1.24__H.A.MATH.OAF.RP.R.L1.25__H.A.MATH.OAF.RP.R.L1.26__H.A.MATH.OAF.RP.R.L1.27__H.A.MATH.OAF.RP.R.L1.3__H.A.MATH.OAF.RP.R.L1.4__H.A.MATH.OAF.RP.R.L1.5__H.A.MATH.OAF.RP.R.L1.6__H.A.MATH.OAF.RP.R.L1.7__H.A.MATH.OAF.RP.R.L1.8__H.A.MATH.OAF.RP.R.L1.9__H.A.MATH.OAF.RP.R.L2.1__H.A.MATH.OAF.RP.R.L2.10__H.A.MATH.OAF.RP.R.L2.11__H.A.MATH.OAF.RP.R.L2.12__H.A.MATH.OAF.RP.R.L2.13__H.A.MATH.OAF.RP.R.L2.14__H.A.MATH.OAF.RP.R.L2.15__H.A.MATH.OAF.RP.R.L2.16__H.A.MATH.OAF.RP.R.L2.17__H.A.MATH.OAF.RP.R.L2.2__H.A.MATH.OAF.RP.R.L2.3__H.A.MATH.OAF.RP.R.L2.4__H.A.MATH.OAF.RP.R.L2.5__H.A.MATH.OAF.RP.R.L2.6__H.A.MATH.OAF.RP.R.L2.7__H.A.MATH.OAF.RP.R.L2.8__H.A.MATH.OAF.RP.R.L2.9__H.A.MATH.OAF.RP.R.L3.1__H.A.MATH.OAF.RP.R.L3.2__H.A.MATH.OAF.RP.R.L3.3__H.A.MATH.OAF.RP.R.L3.4__H.A.MATH.OAF.RP.R.L3.5__H.A.MATH.OAF.RP.R.L3.6__H.A.MATH.OAF.RP.R.L3.7__H.A.MATH.OAF.RP.R.L3.8'
	},
	{
		:label => 'H.A.MATH.OAF.RRT Radical, Rational, and Trigonometric Functions',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.RRT.RRF.L1.1__H.A.MATH.OAF.RRT.RRF.L1.2__H.A.MATH.OAF.RRT.RRF.L1.3__H.A.MATH.OAF.RRT.RRF.L1.4__H.A.MATH.OAF.RRT.RRF.L1.5__H.A.MATH.OAF.RRT.RRF.L1.6__H.A.MATH.OAF.RRT.RRF.L1.7__H.A.MATH.OAF.RRT.RRF.L2.1__H.A.MATH.OAF.RRT.RRF.L2.2__H.A.MATH.OAF.RRT.RRF.L2.3__H.A.MATH.OAF.RRT.RRF.L2.4__H.A.MATH.OAF.RRT.RRF.L2.5__H.A.MATH.OAF.RRT.RRF.L2.6__H.A.MATH.OAF.RRT.RRF.L2.7__H.A.MATH.OAF.RRT.RRF.L2.8__H.A.MATH.OAF.RRT.RRF.L2.9__H.A.MATH.OAF.RRT.TF.L1.1__H.A.MATH.OAF.RRT.TF.L1.2__H.A.MATH.OAF.RRT.TF.L1.3__H.A.MATH.OAF.RRT.TF.L1.4__H.A.MATH.OAF.RRT.TF.L1.5__H.A.MATH.OAF.RRT.TF.L2.1__H.A.MATH.OAF.RRT.TF.L2.2__H.A.MATH.OAF.RRT.TF.L2.3__H.A.MATH.OAF.RRT.TF.L2.4__H.A.MATH.OAF.RRT.TF.L2.5__H.A.MATH.OAF.RRT.TF.L3.1__H.A.MATH.OAF.RRT.TF.L3.2__H.A.MATH.OAF.RRT.TF.L3.3__H.A.MATH.OAF.RRT.TF.L3.4__H.A.MATH.OAF.RRT.TF.L3.5__H.A.MATH.OAF.RRT.TF.L3.6'
	},
	{
		:label => 'H.A.MATH.OAF.SS Sequences and Series',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.SS.SS.L1.1__H.A.MATH.OAF.SS.SS.L1.2__H.A.MATH.OAF.SS.SS.L1.3__H.A.MATH.OAF.SS.SS.L1.4__H.A.MATH.OAF.SS.SS.L1.5__H.A.MATH.OAF.SS.SS.L1.6__H.A.MATH.OAF.SS.SS.L1.7__H.A.MATH.OAF.SS.SS.L1.8__H.A.MATH.OAF.SS.SS.L2.1__H.A.MATH.OAF.SS.SS.L2.2__H.A.MATH.OAF.SS.SS.L2.3__H.A.MATH.OAF.SS.SS.L2.4__H.A.MATH.OAF.SS.SS.L2.5__H.A.MATH.OAF.SS.SS.L2.6__H.A.MATH.OAF.SS.SS.L2.7__H.A.MATH.OAF.SS.SS.L2.8'
	},
	{
		:label => 'H.A.MATH.OAF.VM Vectors and Matrices',
		:url => 'https://www.opened.com/search?standard=H.A.MATH.OAF.VM.M.L1.1__H.A.MATH.OAF.VM.M.L1.2__H.A.MATH.OAF.VM.M.L2.1__H.A.MATH.OAF.VM.M.L2.2__H.A.MATH.OAF.VM.M.L2.3__H.A.MATH.OAF.VM.M.L2.4__H.A.MATH.OAF.VM.M.L3.1__H.A.MATH.OAF.VM.M.L3.2__H.A.MATH.OAF.VM.M.L3.3__H.A.MATH.OAF.VM.V.L1.1__H.A.MATH.OAF.VM.V.L1.2__H.A.MATH.OAF.VM.V.L1.3__H.A.MATH.OAF.VM.V.L1.4__H.A.MATH.OAF.VM.V.L1.5__H.A.MATH.OAF.VM.V.L1.6__H.A.MATH.OAF.VM.V.L1.7__H.A.MATH.OAF.VM.V.L1.8__H.A.MATH.OAF.VM.V.L2.1__H.A.MATH.OAF.VM.V.L2.2__H.A.MATH.OAF.VM.V.L2.3__H.A.MATH.OAF.VM.V.L2.4'
	}
]

@grade = '&grade=8-9-10-11-12'

@video = '&resource_type=video'

@quiz = '&resource_type=assessment'

@hf.each do |skill|

	puts "h4. #{skill[:label]}"

	puts '||Grades||All||Videos||Assessments||'

	@url = "#{skill[:url]}" + '&license=free'

	print '|All|'

	browser.goto @url
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists?
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @video
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @quiz
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		puts '0|'
	else
		puts browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	print '|8,9,10,11,12|'

	browser.goto @url + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @video + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		print '0|'
	else
		print browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end

	browser.goto @url + @quiz + @grade
	Watir::Wait.until { browser.div(:class => "filter-section__center", :index => 0).exists? && browser.div(:class => "filter-section__center", :index => 0).text != "" || browser.div(:class => 'alert alert-info').exists? }
	if browser.div(:class => 'alert alert-info').exists? 
		puts '0|'
	else
		puts browser.div(:class => "filter-section__center", :index => 0).text + '|'
	end
end