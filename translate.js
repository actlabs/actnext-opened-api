'use strict';

var _ = require('lodash');
var mysql = require('mysql');
var data = require('./hf.json');

var connection = mysql.createConnection({
    host: 'radstore.cgvlwwkakzre.us-east-1.rds.amazonaws.com',
    user: 'actlabs',
    password: 'A1989access',
    database: 'raddb'
});

connection.connect();

function processChild(parentCode, node) {
    outputNodeSQL(parentCode, node);

    var nextParentCode = (parentCode === null) ? 'H' : parentCode + '.' + node.code;

    _.forEach(node.children, function(value, key) {
        processChild(nextParentCode, value);
    });
}

function outputNodeSQL(parentCode, node) {

    var code = (parentCode === null) ? 'H' : parentCode + '.' + node.code;
    var desc = _.has(node, 'description') ? node.description : '';

    var post = { code: code, title: node.label , description: desc, parent_code: parentCode};
    var query = connection.query('INSERT INTO hf_skill SET ?', post, function (error, results, fields) {
        if (error) {
            console.log('------------------------------------------------------------');
            console.log(query.sql);
            console.log('------------------------------------------------------------');
            throw error;
        }
    });
    console.log(query.sql); 

    // console.log('insert into hf_skill (code, title, description, parent_code) values ("' + code + '","' + node.label + '","' + desc + '","' + parentCode + '")');
}

_.forEach(data.data, function(value, key) {
    processChild(null, value);
});

connection.end();