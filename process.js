'use strict';

var _ = require('lodash');
var mysql = require('promise-mysql');
var request = require('request-promise');

var assessment_events = require('./assessment-events-output.json');
var clinton_students_assessment_item_events = require('./assessment-item-events-output.json').clinton_students_assessment_item_events;
var media_events = require('./media-events-output.json');

var assessment_item_events = [];
var persons = [];

var connection;
var open_ed_token;


mysql.createConnection({
    host: 'radstore.cgvlwwkakzre.us-east-1.rds.amazonaws.com',
    user: 'actlabs',
    password: 'A1989access',
    database: 'raddb'
}).then(function (conn) { 
    connection = conn; 
    begin();
});

async function begin() {
    // need to unpack this nested array structure...
    for (let clinton_assessment_item_events of clinton_students_assessment_item_events) {
        for (let e of clinton_assessment_item_events.assessment_item_events) {
            assessment_item_events.push(e);
        }
    }

    for (let events of assessment_events.clinton_students_assessment_events) {
        await processAssessmentEvents(events);
    };

    await processMediaEvents();

    if (connection && connection.end) { connection.end(); }
}

async function processAssessmentEvents(events) {
    for (let event of events.assessment_events) {
        await processAssessmentEvent(event);
    };
}

async function processAssessmentEvent(event) {
    var person = await getPerson(event.actor_id);
    var resource = await getOpenEdResource(event.object_id);

    if (resource) {
        var learner_activity = { person_id: person.id, title: 'OpenEd Quiz', description: 'Online OpenEd Quiz', type: 'quiz', weight: 1, date: event.event_time };
        await connection.query('INSERT INTO learner_activity SET ?', learner_activity).then(function (result) {
            learner_activity.id = result.insertId;
        });

        var learning_resource = { learner_activity_id: learner_activity.id, title: resource.title, description: resource.description, urn: event.object_id, format: 'online', type: 'quiz' };
        await connection.query('INSERT INTO learning_resource SET ?', learning_resource).then(function (result) {
            learning_resource.id = result.insertId;
        });

        var items = _.filter(assessment_item_events, { actor_id: event.actor_id, is_part_of: event.object_id });
        for (let item of items) {
            var item_resource = await getOpenEdResource(item.object_id);

            var resource_component = { title: item_resource.title, description: item_resource.description, urn: item.object_id };
            await connection.query('INSERT INTO resource_component SET ?', resource_component).then(function (result) {
                resource_component.id = result.insertId;
            });

            var lr_x_rc = { learning_resource_id: learning_resource.id, resource_component_id: resource_component.id };
            await connection.query('INSERT INTO learning_resource_x_resource_component SET ?', lr_x_rc);

            var outcome = { result: item.generated_score, resource_component_id: resource_component.id, learning_resource_id: learning_resource.id, learner_activity_id: learner_activity.id };
            await connection.query('INSERT INTO outcome SET ?', outcome);
        };
    }
    else {
        console.log("No resource: " + event.object_id)
    }
}

async function processMediaEvents() {
    for (let event of media_events) {
        await processMediaEvent(event);
    }
}

async function processMediaEvent(event) {
    var person = await getPerson(event.actor_id);
    var resource = await getOpenEdResource(event.object_id);

    if (resource) {
        var learner_activity = { person_id: person.id, title: 'OpenEd Video', description: 'Online OpenEd Video', type: 'video', weight: 0.01, date: event.created_at };
        await connection.query('INSERT INTO learner_activity SET ?', learner_activity).then(function (result) {
            learner_activity.id = result.insertId;
        });

        var learning_resource = { learner_activity_id: learner_activity.id, title: resource.title, description: resource.description, urn: event.object_id, format: 'online', type: 'video' };
        await connection.query('INSERT INTO learning_resource SET ?', learning_resource).then(function (result) {
            learning_resource.id = result.insertId;
        });

        var outcome = { result: event.action, learning_resource_id: learning_resource.id, learner_activity_id: learner_activity.id };
        await connection.query('INSERT INTO outcome SET ?', outcome);
    }
    else {
        console.log("No resource: " + event.object_id)
    }
}

async function getOpenEdToken() {
    if (open_ed_token) {
        return open_ed_token;
    } else {
        var token_body = await request({
            method: 'POST',
            url: 'https://partner.opened.com/1/oauth/get_token',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
        });
        open_ed_token = JSON.parse(token_body).access_token;
        return open_ed_token;
    }
}

async function getOpenEdResource(urn) {
    await getOpenEdToken();

    var resource_id = parseInt(urn.split('/').pop());
    try {
        var resource_body = await request({
            method: 'GET',
            url: 'https://partner.opened.com/1/resources/' + resource_id.toString(),
            headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + open_ed_token }
        });
        return JSON.parse(resource_body).resource;

    } catch (error) {
        return undefined;
    }
}

async function getPerson(act_id) {
    var p = _.find(persons, { 'act_id': act_id });
    if (p === undefined) {
        await getOpenEdToken();
        var user_id = parseInt(act_id.substr(act_id.length - 7));
        var user_body = await request({ 
                method: 'PUT', 
                url: 'https://partner.opened.com/1/users/' + user_id.toString(),
                headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + open_ed_token }
        });
        var user = JSON.parse(user_body).user;

        p = { act_id: act_id, first_name: 'first', last_name: 'last', email: user.email };
        await connection.query('INSERT INTO person SET ?', p).then(function(result) {
            p.id = result.insertId;
            persons.push(p);
            return p;
        });
    }
    return p;
}