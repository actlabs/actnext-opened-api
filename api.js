'use strict';

var http = require('http');
var qs = require('querystring');
var url = require('url');
var ip = require('ip');
var Q = require('q');
var _ = require('lodash');
var request = require('request');

var token = null;


function contentSearch(req, res, searchString) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/resources.json/?limit=10&descriptive=' + encodeURI(searchString),
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token }
		},
		function (error, response, body) {
			return res.end(body)
		});
	});
}

function standardGroups(req, res) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/standard_groups.json',
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token }
		},
		function (error, response, body) {
			return res.end(body)
		});
	});
}

function categories(req, res, standardGroup) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/categories.json?standard_group=' + encodeURI(standardGroup),
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token }
		},
		function (error, response, body) {
			return res.end(body)
		});
	});
}

function standards(req, res, category) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/standards.json?category=' + encodeURI(category),
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token }
		},
		function (error, response, body) {
			return res.end(body)
		});
	});
}

function searchByStandard(req, res, standard) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/resources.json/?limit=5&standard_group=31',
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token }
		},
		function (error, response, body) {
			return res.end(body)
		});
	});
}

function createUser(req, res, email, username) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'POST',
			url: 'https://partner.opened.com/1/users',
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token },
			body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"email\": \"" + email + "\", \"username\": \"" + username + "\", \"password\": \"actnext\", \"role\": \"student\"}"
		},
		function (error, response, body) {
			return res.end(body);
		});
	});
}


function updateUser(req, res, id) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'PUT',
			url: 'https://partner.opened.com/1/users/' + id,
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token },
			body: "{ }"
		},
		function (error, response, body) {
			return res.end(body);
		});
	});
}

function searchUser(req, res, email) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/users/search?username=' + encodeURI(email),
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token }
		},
		function (error, response, body) {
			return res.end(body);
		});
	});
}

function getUserAssessments(req, res) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"kurtpete2\", \"role\": \"student\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/assessment_runs.json/?resource_id=9056403',
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token },
		},
		function (error, response, body) {
			console.log(response)
			if (error)
				return res.end(error);
			return res.end(body);
		});
	});
}

function getResource(req, res, id) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' },
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/resources/' + id.toString(),
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token },
		},
			function (error, response, body) {
				return res.end(body);
			});
	});
}

function getAnswer(req, res, id) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' },
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
	}, function (error, response, body) {
		token = JSON.parse(body).access_token;

		request({
			method: 'GET',
			url: 'https://partner.opened.com/1/users/' + id.toString(),
			headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + token },
			body: "{  \"school_nces_id\": \"450264000708\"}"
		},
			function (error, response, body) {
				return res.end(body);
			});
	});
}


function getStudentToken(req, res) {
	request({
		method: 'POST',
		url: 'https://partner.opened.com/1/oauth/get_token',
		headers: { 'Content-Type': 'application/json; charset=utf-8' }, 
		body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\",  \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\", \"username\": \"kurtpete2\", \"role\": \"student\"}"
	}, function (error, response, body) {
		return res.end(body);
	});
}

function route(req, res) {

	// extremely simple url based api
	var inUrl = url.parse(req.url, true);
	var command = inUrl.pathname.substr(1);


	if (command) {

		// jsonp
		if (inUrl.query.callback === undefined || inUrl.query.callback === '') {
			inUrl.query.callback = 'callback';
		}

		switch (command) {
			case 'contentSearch': {
				if (inUrl.query.searchString) {
					return contentSearch(req, res, inUrl.query.searchString);
				}
				break;
			}
			case 'standardGroups': {
				return standardGroups(req, res);

				break;
			}
			case 'categories': {
				if (inUrl.query.standardGroup) {
					return categories(req, res, inUrl.query.standardGroup);
				}
				break;
			}
			case 'standards': {
				if (inUrl.query.category) {
					return standards(req, res, inUrl.query.category);
				}
				break;
			}
			case 'searchByStandard': {
				if (inUrl.query.standard) {
					return searchByStandard(req, res, inUrl.query.standard);
				}
				break;
			}
			case 'createUser': {
				if (inUrl.query.email && inUrl.query.username) {
					return createUser(req, res, inUrl.query.email, inUrl.query.username);
				}
				break;
			}
			case 'searchUser': {
				if (inUrl.query.email) {
					return searchUser(req, res, inUrl.query.email);
				}
				break;
			}
			case 'updateUser': {
				if (inUrl.query.id) {
					return updateUser(req, res, inUrl.query.id);
				}
				break;
			}
			case 'getResource': {
				if (inUrl.query.id) {
					return getResource(req, res, inUrl.query.id);
				}
				break;
			}
			case 'deleteUser': {
				return deleteUser(req, res);
				break;
			}
			case 'getStudentToken': {
				return getStudentToken(req, res);
				break;
			}
			case 'getUserAssessments': {
				return getUserAssessments(req, res);
				break;
			}						
			default: {
				break;
			}
		}
	}

	res.end('{}');
}

//contentSearch('World War II');

var d = require('domain').create();

d.on('error', function(e) {
	console.log(e);
});

d.run(function() {
	http.createServer(function (req, res) {
		res.writeHead(200, {'Content-Type': 'application/json'});
		route(req, res);
	}).listen(4444, '0.0.0.0');
});

console.log('Server running at http://' + ip.address() + ':4444/');