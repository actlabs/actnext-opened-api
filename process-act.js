'use strict';

var _ = require('lodash');
var mysql = require('promise-mysql');
var request = require('request-promise');

var act_scores = require('./clinton-act-scores.json');

var connection;
var open_ed_token;

mysql.createConnection({
    host: 'radstore.cgvlwwkakzre.us-east-1.rds.amazonaws.com',
    user: 'actlabs',
    password: 'A1989access',
    database: 'raddb'
}).then(function (conn) { 
    connection = conn; 
    begin();
});

async function begin() {
    for (let score of act_scores) {
        var person = await getPerson(score);
        await processScore(score, person);
    };

    if (connection && connection.end) { connection.end(); }
}

async function processScore(score, person) {
    // hardcoding known date.  need a what to bring this in via process.  test date was: December 9, 2017
    var learner_activity = { person_id: person.person_id, title: 'ACT', description: 'ACT Test Administration', type: 'assessment', weight: 1, date: '2017-12-09T12:00:00.000Z' };
    await connection.query('INSERT INTO learner_activity SET ?', learner_activity).then(function (result) {
        learner_activity.id = result.insertId;
    });

    var r = {
        raw_score_english: score.RAW_SCORE_ENGLISH,
        raw_score_math: score.RAW_SCORE_MATH,
        raw_score_reading: score.RAW_SCORE_READING,
        raw_score_science: score.RAW_SCORE_SCIENCE,
        scale_score_composite: score.SCALE_SCORE_COMPOSITE,
        scale_score_english: score.SCALE_SCORE_ENGLISH,
        scale_score_math: score.SCALE_SCORE_MATH,
        scale_score_reading: score.SCALE_SCORE_READING,
        scale_score_science: score.SCALE_SCORE_SCIENCE,
        test_item_resp_english: score.TEST_ITEM_RESP_ENGLISH,
        test_item_resp_math: score.TEST_ITEM_RESP_MATH,
        test_item_resp_reading: score.TEST_ITEM_RESP_READING,
        test_item_resp_science: score.TEST_ITEM_RESP_SCIENCE,
        score_vector_english: score.SCORE_VECTOR_ENGLISH,
        score_vector_math: score.SCORE_VECTOR_MATH,
        score_vector_reading: score.SCORE_VECTOR_READING,
        score_vector_science: score.SCORE_VECTOR_SCIENCE
    };

    var outcome = { result: JSON.stringify(r), learner_activity_id: learner_activity.id };
    await connection.query('INSERT INTO outcome SET ?', outcome);
}

async function getOpenEdToken() {
    if (open_ed_token) {
        return open_ed_token;
    } else {
        var token_body = await request({
            method: 'POST',
            url: 'https://partner.opened.com/1/oauth/get_token',
            headers: { 'Content-Type': 'application/json; charset=utf-8' },
            body: "{  \"client_id\": \"1efe064761db70dcddf60d287c99e44e59679f4c16e64bd07c9db865bcb9c414\", \"secret\": \"b527a03336e390303f6d71e2cb4daa08f5c7c0cd0da9a14514aaebbad60c146c\",  \"username\": \"actlabs\"}"
        });
        open_ed_token = JSON.parse(token_body).access_token;
        return open_ed_token;
    }
}

async function getPerson(score) {
    var row = await connection.query("select * from person where email = lower('" + score.EMAIL + "')");
    var p = {};
    if (row.length === 0) {
        await getOpenEdToken();

        var user_body = await request({
            method: 'GET',
            url: 'https://partner.opened.com/1/users/search?username=' + score.EMAIL.toString(),
            headers: { 'Content-Type': 'application/json; charset=utf-8', 'Authorization': 'Bearer ' + open_ed_token }
        });
        var user = JSON.parse(user_body).user;
        if (user) {
            p = {
                act_id: 'https://www.opened.com/user/' + user.id,
                first_name: score.FIRST_NAME_LONG,
                last_name: score.LAST_NAME_LONG,
                email: user.email
            };
        }
        else {
            p = {
                act_id: 'https://www.opened.com/user/' + score.EMAIL,
                first_name: score.FIRST_NAME_LONG,
                last_name: score.LAST_NAME_LONG,
                email: score.EMAIL
            };
        }

        await connection.query('INSERT INTO person SET ?', p).then(function (result) {
            p.person_id = result.insertId;
            return p;
        });
    }
    else {
        p = row[0];
        p.first_name = score.FIRST_NAME_LONG;
        p.last_name = score.LAST_NAME_LONG;
        await connection.query('UPDATE person SET ? WHERE person_id = ?', [p, p.person_id]).then(function (result) {
            return p;
        });
    }

    return p;
}