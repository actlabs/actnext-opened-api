'use strict';

var http = require('http');
var qs = require('querystring');
var url = require('url');
var ip = require('ip');
var Q = require('q');
var _ = require('lodash');
var request = require('request');
const fs = require('fs');

var token = null;


function queryAssessmentEvents(req, res, id, page) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, 
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		var p = (page) ? page : 1; 

		request({
			method: 'GET',
			url: 'https://www.opencallisto.org/assessment_events.json?actor_id=' + encodeURI('https://www.opened.com/user/' + id + '&page=' + p),
			headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
		},
		function (error, response, body) {
			console.log('---------------------------- user ' + id + ' body ----------------------------');
			console.log(body);
			return res.end(body)
		});
	});
}



function queryAssessmentItemEvents(req, res, id, page) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, 
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		var p = (page) ? page : 1; 

		request({
			method: 'GET',
			url: 'https://www.opencallisto.org/assessment_item_events.json?actor_id=' + encodeURI('https://www.opened.com/user/' + id + '&page=' + p),
			headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
		},
		function (error, response, body) {
			console.log('---------------------------- user ' + id + ' body ----------------------------');
			console.log(body);
			return res.end(body)
		});
	});
}


function queryAllAssessmentItemEvents(req, res, page) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, 
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		var p = (page) ? page : 1; 

		request({
			method: 'GET',
			url: 'https://www.opencallisto.org/assessment_item_events.json?page=' + p,
			headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
		},
		function (error, response, body) {
			//console.log('---------------------------- user ' + id + ' body ----------------------------');
			console.log(body);
			return res.end(body)
		});
	});
}
function range1(i){return i?range1(i-1).concat(i):[]}

async function queryAssessmentItemEventsByObjectId(req, res, id, page) {
	var p = (page) ? page : 1; 
	//console.log('page: ' + p)
    request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }, 
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, async function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		//var p = (page) ? page : 1; 

		request({
			method: 'GET',
			url: 'https://www.opencallisto.org/assessment_item_events.json?object_id=' + encodeURI('https://www.opened.com/resources/' + id + "&page=" + p),
			headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
		},
		 async function (error, response, body) {
			//console.log('---------------------------- user ' + id + ' body ----------------------------');
			//console.log(body);
			var obj = JSON.parse(body);
			if (typeof obj.assessment_item_events != 'undefined') {
			  for (var i = 0; i < obj.assessment_item_events.length; i++) {
				  console.log(obj.assessment_item_events[i].actor_id)
				  fs.appendFileSync('results.csv', obj.assessment_item_events[i].object_id + ',' + obj.assessment_item_events[i].actor_id + ',' + obj.assessment_item_events[i].generated_score + ',' + obj.assessment_item_events[i].event_time +'\n');
			  }
			}
			//console.log(JSON.stringify(obj))
			//console.log('Pages: ' + obj.pagination.page_entries);
			if ((p === 1) && (typeof obj.pagination != 'undefined')) {
				
				var pages = range1(obj.pagination.total_pages);
				pages.shift();
				console.log(pages)
				for (const item of pages) {
					// each page
					await request({
								method: 'GET',
								url: 'https://www.opencallisto.org/assessment_item_events.json?object_id=' + encodeURI('https://www.opened.com/resources/' + id + "&page=" + item),
								headers: { 'Content-Type': 'application/json', 'uid': uid, 'client': client, 'access-token': access_token }
							},
							  function (error, response, body) {
								var obj = JSON.parse(body);
								if (typeof obj.assessment_item_events != 'undefined') {
								  for (var i = 0; i < obj.assessment_item_events.length; i++) {
									  console.log(obj.assessment_item_events[i].actor_id)
									  fs.appendFileSync('results.csv', obj.assessment_item_events[i].object_id + ',' + obj.assessment_item_events[i].actor_id + ',' + obj.assessment_item_events[i].generated_score + ',' + obj.assessment_item_events[i].event_time +'\n');
								  }
						        }
					          })
				}

		    }
			return res.end(body)
		});
	});
}


function queryMediaEvents(req, res, id) {
	request({
		method: 'POST',
		url: 'https://www.opencallisto.org/auth/sign_in',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json'
		},
		body: "{  \"email\": \"lucas@opened.com\",  \"password\": \"opencallisto78965\"}"
	}, function (error, response, body) {

		var uid = response.headers['uid'];
		var client = response.headers['client'];
		var access_token = response.headers['access-token'];

		request({
				method: 'GET',
			url: 'https://www.opencallisto.org/media_events.json?actor_id=' + encodeURI('https://www.opened.com/user/' + id),
				headers: {
					'Content-Type': 'application/json',
					'uid': uid,
					'client': client,
					'access-token': access_token
				}
			},
			function (error, response, body) {
				return res.end(body)
			});
	});
}


async function route(req, res) {

	// extremely simple url based api
	var inUrl = url.parse(req.url, true);
	var command = inUrl.pathname.substr(1);

	if (command) {

		// jsonp
		if (inUrl.query.callback === undefined || inUrl.query.callback === '') {
			inUrl.query.callback = 'callback';
		}

		switch (command) {
			case 'queryAssessmentEvents': {
				if (inUrl.query.id) {
					return queryAssessmentEvents(req, res, inUrl.query.id, inUrl.query.page);
				}
				break;
			}
			case 'queryAssessmentItemEvents': {
				if (inUrl.query.id) {
					return queryAssessmentItemEvents(req, res, inUrl.query.id, inUrl.query.page);
				}
				break;
			}
			case 'queryAllAssessmentItemEvents': {
				return queryAllAssessmentItemEvents(req, res, inUrl.query.page);
				break;
			}
			case 'queryAssessmentItemEventsByObjectId': {
				if (inUrl.query.id) {
					/** * * * * *** **
					 Sometimes Objects are Composite which means we need to find the sub items we need to ouse to query open callisto instead.
					 This code determines if that is the casee by using the resource API and then locats the child ids we should be using */
					//go fetch resource information. if obj.question.type == CompositeQuestion
					//e.g. https://www.opened.com/resourceapi/1/questions/9436002.json

					//var formids = [9436002,9448242,9436008,9436014,9448199,9448205,9448248,9448211,9448217,9448223,9448229,9448235,9448254,9448261,9448267,9449359,9449362,9451386,9449364,9449366,9449371,9449374,9451388,9451091,9451373,9451390,9451392,9451394,9451396,9451398,9451764,9451765,9451767,9451789,9451776,9451791,9451778,9451793,9451795,9451782,9451797,9451784,9451786,9451799,9451801,9452035,9452036,9452121,9452039,9452041,9452123,9452125,9452044,9452072,9452127,9452130,9452132,9452135,9452137,9452139,9452468,9452472,9452474,9452478,9452502,9452480,9452482,9452510,9452484,9452519,9452576,9452495,9452499,9452486,9452578]

					//for (const item of formids) {

					await request({
							method: 'GET',
							url: 'https://www.opened.com/resourceapi/1/questions/' + inUrl.query.id + '.json',
							headers: {
								'Accept': 'application/json',
								'Content-Type': 'application/json'
							}
						}, async function (error, response, body) {

							 var obj = JSON.parse(body);
							 //console.log('i see:' + obj.question.type);
							 if (typeof obj.question != 'undefined') {

							 if(obj.question.type == 'CompositeQuestion') {
								 for (var i = 0; i < obj.question.child_ids.length; i++) {
								 	await queryAssessmentItemEventsByObjectId(req, res, obj.question.child_ids[i]);
								 }
								 return {};
					          } else {
							   return await queryAssessmentItemEventsByObjectId(req, res, inUrl.query.id);
							  }
							} else {
								fs.appendFileSync('errors.csv', inUrl.query.id + '\n');
							}
							
						});
				     //}
		              /** * * * * *** **/
					  //return queryAssessmentItemEventsByObjectId(req, res, inUrl.query.id);
				}
				break;
			}
			case 'childIds': {
				if (inUrl.query.id) {

					await request({
							method: 'GET',
							url: 'https://www.opened.com/resourceapi/1/questions/' + inUrl.query.id + '.json',
							headers: {
								'Accept': 'application/json',
								'Content-Type': 'application/json'
							}
						}, async function (error, response, body) {

							 var obj = JSON.parse(body);
							 //console.log('i see:' + obj.question.type);
							 if(obj.question.type == 'CompositeQuestion') {
								 for (var i = 0; i < obj.question.child_ids.length; i++) {
								 	//await queryAssessmentItemEventsByObjectId(req, res, obj.question.child_ids[i]);
								 	fs.appendFileSync('childids.csv', inUrl.query.id + ',' + obj.question.child_ids[i] +'\n');
								 }
								 return {};
					          }
							
						});
				     //}
		              /** * * * * *** **/
					  //return queryAssessmentItemEventsByObjectId(req, res, inUrl.query.id);
				}
				break;
			}
			case 'queryMediaEvents':
				{
					if (inUrl.query.id) {
						return queryMediaEvents(req, res, inUrl.query.id);
					}
					break;
				}
			default: {
				break;
			}
		}
	}

	res.end('{}');
}

//contentSearch('World War II');

var d = require('domain').create();

d.on('error', function(e) {
	console.log(e);
});

d.run(function() {
	http.createServer(function (req, res) {
		res.writeHead(200, {'Content-Type': 'application/json'});
		route(req, res);
	}).listen(4444, '0.0.0.0');
});

console.log('Server running at http://' + ip.address() + ':4444/');